/**
 * 
 */
package it.polimi.cg16.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import it.polimi.cg16.model.Color;
import it.polimi.cg16.model.Region;
import it.polimi.cg16.model.Terrain;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Michele
 * 
 */
public class PlayerClientTest {
    PlayerClient player;

    @Before
    public void setUp() {
        player = new PlayerClient("Prova");
    }

    /**
     * test for setChosenShepherd method and its getter
     */
    @Test
    public void testSetChosenShepherd() {
        player.setChosenShepherd(0);
        assertEquals("Chosen shepherd isn't correct", 0, player.getChosenShepherd());
    }

    /**
     * Test for setMoney method and its getter
     */
    @Test
    public void testSetMoney() {
        player.setMoney(20);
        assertEquals("Money are set incorreclt", 20, player.getMoney());
    }

    /**
     * Test for setTerrainCard method and its getter
     */
    @Test
    public void testSetTerrainCard() {
        List<Card> cards = new ArrayList<>();
        cards.add(new Card(Terrain.PLAIN, 0));
        player.setTerrainCards(cards);
        assertSame("Terrain cards set incorrectly", cards.get(0), player.getTerrainCards().get(0));
    }

    /**
     * Test for setShepherd method and its getter
     */
    @Test
    public void testSetShepherd() {
        List<ShepherdClient> shepherds = new ArrayList<>();
        shepherds.add(new ShepherdClient(new CellClient(0, new Region(1), new Region(1)), Color.BLUE));
        player.setShepherds(shepherds);
        assertSame("Shepherd set incorrectly", shepherds.get(0), player.getShepherds().get(0));
    }

}
