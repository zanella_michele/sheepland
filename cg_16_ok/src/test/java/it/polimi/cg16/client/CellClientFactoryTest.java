/**
 * 
 */
package it.polimi.cg16.client;

import static it.polimi.cg16.client.GameBoardClient.NUMBER_OF_CELL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import it.polimi.cg16.model.Region;
import it.polimi.cg16.model.RegionFactory;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Michele
 * 
 */
public class CellClientFactoryTest {
    private Region[] region;
    private CellClient[] cell;

    @Before
    public void setUp() {
        RegionFactory regionFactory = new RegionFactory();
        region = regionFactory.regionCreator();
        CellClientFactory cellFactory = new CellClientFactory();
        cell = cellFactory.cellsCreator(region);
    }

    /**
     * Test for cellsCreator method
     */
    @Test
    public void testId() {
        // Pass all the cells created and verify if different cells have the
        // same id
        for (int i = 0; i < NUMBER_OF_CELL; i++) {
            for (int j = 0; j < NUMBER_OF_CELL; j++) {
                if (i == j) {
                    assertEquals("Id should be the same", cell[i], cell[j]);
                } else {
                    assertFalse("Id should be different if regions are different", cell[i] == cell[j]);
                }
            }
        }
    }
}
