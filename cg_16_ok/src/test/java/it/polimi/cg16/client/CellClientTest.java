/**
 * 
 */
package it.polimi.cg16.client;

import static it.polimi.cg16.client.CellClient.FREE;
import static it.polimi.cg16.client.CellClient.LOCK;
import static it.polimi.cg16.model.Terrain.COUNTRY;
import static it.polimi.cg16.model.Terrain.MOUNTAIN;
import static it.polimi.cg16.model.Terrain.PLAIN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import it.polimi.cg16.model.Region;

import org.junit.Test;

/**
 * @author Michele
 * 
 */
public class CellClientTest {

    /**
     * Test some setter and getter methods
     */
    @Test
    public void testSetterAndGetter() {
        Region region1 = new Region(1, COUNTRY);
        Region region2 = new Region(2, PLAIN);
        CellClient cell = new CellClient(0, region1, region2);
        // Check if cell is correctly created
        assertEquals("Id is incorrect", 0, cell.getId());
        assertSame("Region 1 is incorrect", region1, cell.getRegion1());
        assertSame("Region 2 is incorrect", region2, cell.getRegion2());
        assertEquals("State should be FREE", FREE, cell.getState());
        // Check setState
        cell.setState(LOCK);
        assertEquals("State isn't set correctly", LOCK, cell.getState());
        // Check setRegion1
        Region region3 = new Region(3, MOUNTAIN);
        cell.setRegion1(region3);
        assertSame("Region 1 isn't set correctly", region3, cell.getRegion1());
        // Check setRegion2
        cell.setRegion2(region3);
        assertSame("Region 2 isn't set correctly", region3, cell.getRegion2());
    }
}
