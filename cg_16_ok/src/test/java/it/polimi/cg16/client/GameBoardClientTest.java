/**
 * 
 */
package it.polimi.cg16.client;

import static it.polimi.cg16.client.GameBoardClient.NUMBER_OF_CELL;
import static it.polimi.cg16.client.GameBoardClient.NUMBER_OF_REGION;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import it.polimi.cg16.model.Color;
import it.polimi.cg16.model.Region;
import it.polimi.cg16.model.Sheep;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Michele
 * 
 */
public class GameBoardClientTest {
    private GameBoardClient gameboard;

    @Before
    public void setUp() {
        gameboard = new GameBoardClient();
    }

    /**
     * Test if the gameboard is correctly created
     */
    @Test
    public void testGameBoardClient() {
        // Check if regions are created correctly
        assertTrue("Regions are not created correctly", gameboard.getRegions().length == NUMBER_OF_REGION + 1);
        assertNotSame("Regions should be different", gameboard.getRegions()[0], gameboard.getRegions()[1]);
        // Check if cells are created correctly
        assertTrue("Cells not created correctly", gameboard.getCells().length == NUMBER_OF_CELL);
        assertNotSame("Cells should be different", gameboard.getCells()[0], gameboard.getCells()[1]);
    }

    /**
     * Test for setDeckView and its getter methods
     */
    @Test
    public void testSetDeckView() {
        // Check if setDeckView works correctly
        List<Card> deck = new ArrayList<Card>();
        gameboard.setDeckClient(deck);
        assertSame("Deck isn't correctly set", deck, gameboard.getDeckClient());
    }

    /**
     * Test for setRegionsSheeps and its getter methods
     */
    @Test
    public void testSetRegionsSheeps() {
        // Check if SetRegionsSheeps works correctly
        List<Sheep> sheeps = new ArrayList<Sheep>();
        sheeps.add(new Sheep(1, gameboard.getRegions()[1]));
        gameboard.setRegionsSheeps(1, sheeps);
        assertSame("Sheep isn't in the correct region", sheeps.get(0), gameboard.getRegions()[1].getSheep().get(0));
    }

    /**
     * Test for getBlackSheepIdRegion method
     */
    @Test
    public void testGetBlackSheepIdRegion() {
        assertEquals("Region of the blacksheep should have id 0 after gameboard creation", 0, gameboard.getBlackSheepIdRegion());
    }

    /**
     * Test for setBlackSheep method
     */
    @Test
    public void testSetBlackSheep() {
        gameboard.setBlackSheep(1);
        assertEquals("Region of blacksheep set incorrectly", 1, gameboard.getBlackSheepIdRegion());
    }

    /**
     * Test for otherShepherd getter and setter methods
     */
    @Test
    public void testOtherShepherds() {
        List<ShepherdClient> shepherds = new ArrayList<>();
        shepherds.add(new ShepherdClient(new CellClient(0, new Region(0), new Region(1)), Color.RED));
        gameboard.setOtherShepherds(shepherds);
        assertSame("Other shepherds set incorrectly", shepherds, gameboard.getOtherShepherds());
    }

}
