/**
 * 
 */
package it.polimi.cg16.client;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author Michele
 * 
 */
public class DiceClientTest {

    /**
     * test for numExtracted setter and getter methods
     */
    @Test
    public void testSetterAndGetter() {
        DiceClient dice = new DiceClient();
        dice.setNumExtracted(5);
        assertEquals("Actual extracted number incorrect", 5, dice.getNumExtracted());
    }

}
