/**
 * 
 */
package it.polimi.cg16.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import it.polimi.cg16.model.Color;
import it.polimi.cg16.model.Region;
import it.polimi.cg16.model.Terrain;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Michele
 * 
 */
public class ShepherdClientTest {
    private ShepherdClient shepherd;
    private ShepherdClient shepherd2;

    @Before
    public void setUp() {
        shepherd = new ShepherdClient(new CellClient(0, new Region(1, Terrain.PLAIN), new Region(2, Terrain.DESERT)), Color.BLUE);
        shepherd2 = new ShepherdClient(0, new CellClient(0, new Region(1, Terrain.PLAIN), new Region(2, Terrain.DESERT)), Color.BLUE);
    }

    /**
     * Test for cell getter and setter methods
     */
    @Test
    public void testCell() {
        CellClient cell = new CellClient(3, new Region(4, Terrain.PLAIN), new Region(3, Terrain.COUNTRY));
        // Check if setCell works correctly
        shepherd.setCell(cell);
        assertSame("cell isn't set correctly", cell, shepherd.getCell());
    }

    /**
     * Test for color getter and setter methods
     */
    @Test
    public void testColor() {
        shepherd2.setColor(Color.GREEN);
        assertSame("Color not set", Color.GREEN, shepherd2.getColor());
    }

    /**
     * Test for getId method
     */
    @Test
    public void testGetId() {
        assertEquals("Id should be 0", 0, shepherd2.getId());
    }

}
