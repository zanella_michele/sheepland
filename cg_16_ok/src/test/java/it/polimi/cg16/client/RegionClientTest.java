/**
 * 
 */
package it.polimi.cg16.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import it.polimi.cg16.model.Region;
import it.polimi.cg16.model.Sheep;
import it.polimi.cg16.model.Terrain;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * @author Michele
 * 
 */
public class RegionClientTest {

    /**
     * Test some setter and getter methods
     */
    @Test
    public void testSetterAndGetter() {
        Region region = new Region(0, Terrain.COUNTRY);
        // Test getId
        assertEquals("Id set incorrectly", 0, region.getId());
        // Test getTerrain
        assertEquals("Terrain set incorrectly", Terrain.COUNTRY, region.getTerrain());
        // Test setSheeps and getSheeps
        List<Sheep> sheeps = new ArrayList<Sheep>();
        sheeps.add(new Sheep(0, region));
        region.setSheeps(sheeps);
        assertSame("Sheeps set incorrectly", sheeps.get(0), region.getSheep().get(0));
    }

}
