/**
 * 
 */
package it.polimi.cg16.client;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author Michele
 * 
 */
public class MoveClientTest {

    /**
     * Test methods of MoveClient
     */
    @Test
    public void testMoveClient() {
        int i = 0;
        String name[] = { "Move Sheep", "Move Shepherd", "Buy a terrain card", "Kill a sheep", "Generate new sheep" };
        for (MoveClient move : MoveClient.values()) {
            assertEquals("Type incorrect", i, move.getType());
            assertEquals("Description incorrect", name[i], move.getDescription());
            i++;
        }
    }

}
