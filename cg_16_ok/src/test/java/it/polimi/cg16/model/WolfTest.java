/**
 * 
 */
package it.polimi.cg16.model;

import static org.junit.Assert.assertSame;

import org.junit.Test;

/**
 * @author Michele
 * 
 */
public class WolfTest {

    /**
     * Test Region getter and setter method
     */
    @Test
    public void testRegion() {
        Wolf wolf = new Wolf(new Region(1));
        Region region = new Region(0);
        wolf.setRegion(region);
        assertSame("Region is set incorrectly", region, wolf.getRegion());
    }

}
