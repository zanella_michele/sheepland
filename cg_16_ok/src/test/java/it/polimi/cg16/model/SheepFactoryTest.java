/**
 * 
 */
package it.polimi.cg16.model;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * @author Michele
 * 
 */
public class SheepFactoryTest {

    /**
     * Check if the factory assigns the correct and different id
     */
    @Test
    public void testFactory() {
        SheepFactory factory = new SheepFactory();
        Sheep sheep1 = factory.getSheep(new Region(0));
        Sheep sheep2 = factory.getSheep(new Region(1));
        assertTrue("Id are incorrect", sheep1.getId() != sheep2.getId());
    }

}
