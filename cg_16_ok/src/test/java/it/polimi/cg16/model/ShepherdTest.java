/**
 * 
 */
package it.polimi.cg16.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import it.polimi.cg16.model.Cell;
import it.polimi.cg16.model.Color;
import it.polimi.cg16.model.Region;
import it.polimi.cg16.model.Shepherd;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Michele
 * 
 */
public class ShepherdTest {
    private Shepherd shepherd;

    Cell cell;

    @Before
    public void setUp() {
        shepherd = new Shepherd(Color.RED);
    }

    /**
     * Test if the constructor works properly test getTerrainCards method
     */
    @Test
    public void testShepherd() {
        assertNotNull("shepherd must be instantiate", shepherd);
        assertSame("Color set incorrectly", Color.RED, shepherd.getColor());
    }

    /**
     * test setCell and getCell method
     */
    @Test
    public void testSetCell() {
        cell = new Cell(0, new Region(1), new Region(2), 0);

        // Check if after creation cell is null
        assertNull("Cell should be null after creation", shepherd.getCell());

        // set a cell
        shepherd.setCell(cell);

        // Check if setCell worked properly
        assertEquals("Cell is not set correctly", cell, shepherd.getCell());
    }

}
