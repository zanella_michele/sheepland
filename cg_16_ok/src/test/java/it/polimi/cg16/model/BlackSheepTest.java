/**
 * 
 */
package it.polimi.cg16.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import it.polimi.cg16.model.BlackSheep;
import it.polimi.cg16.model.Region;
import it.polimi.cg16.model.Terrain;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Michele
 * 
 */
public class BlackSheepTest {
	private static Region sheepsburg;
	private static Region casualRegion;
	private BlackSheep blackSheep;

	/**
	 * 
	 */
	@BeforeClass
	public static void setUp() {
		sheepsburg = new Region(0);
		casualRegion = new Region(1, Terrain.PLAIN);
	}

	/**
	 * Test the constructor of BlackSheep
	 */
	@Test
	public void testBlackSheep() {
		blackSheep = new BlackSheep(0, sheepsburg);
		// Check if constructor of BlackSheep work correctly
		assertNotNull("blackSheep should be instantiated here", blackSheep);

		// Check if the start region is set correctly by the constructor
		assertEquals("Sheepsburg region isn't correctly set", sheepsburg,
				blackSheep.getRegion());
	}

	/**
	 * test setRegion method
	 */
	@Test
	public void testSetRegion() {
		blackSheep = new BlackSheep(0, sheepsburg);
		// set region
		blackSheep.setRegion(casualRegion);

		// Check if region is not null after setRegion
		assertNotNull("region must not be NULL here", blackSheep.getRegion());

		// Check if region in BlackSheep is the same of mine
		assertSame("Both object should be the same", casualRegion,
				blackSheep.getRegion());
	}

}
