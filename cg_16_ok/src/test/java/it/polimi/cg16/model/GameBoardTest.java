/**
 * 
 */
package it.polimi.cg16.model;

import static it.polimi.cg16.model.GameBoard.MAX_FENCES;
import static it.polimi.cg16.model.GameBoard.MAX_MONEY_2_PLAYERS;
import static it.polimi.cg16.model.GameBoard.MOVE_SHEPHERD_FAR;
import static it.polimi.cg16.model.GameBoard.NUMBER_OF_CELL;
import static it.polimi.cg16.model.GameBoard.NUMBER_OF_REGION;
import static it.polimi.cg16.model.GameBoard.SHEEPSBURG;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import it.polimi.cg16.model.BlackSheep;
import it.polimi.cg16.model.Card;
import it.polimi.cg16.model.Cell;
import it.polimi.cg16.model.GameBoard;
import it.polimi.cg16.model.Player;
import it.polimi.cg16.model.Region;
import it.polimi.cg16.model.Sheep;
import it.polimi.cg16.model.Shepherd;
import it.polimi.cg16.model.Player.NoMoneyException;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Michele
 * 
 */
public class GameBoardTest {

    private static final int NUMBER_OF_PLAYER = 2;

    private GameBoard gameBoard2Players;
    private GameBoard gameBoard4Players;
    private Player actualPlayer;
    private Cell chosenCell;

    @Before
    public void setUp() {
        gameBoard2Players = new GameBoard(NUMBER_OF_PLAYER);
        actualPlayer = gameBoard2Players.getPlayer()[0];
        chosenCell = gameBoard2Players.getCell()[0];
    }

    /**
     * Test if the gameboard is correctly created
     */
    @Test
    public void testGameBoard() {

        gameBoard4Players = new GameBoard(4);

        // Check if Regions are created correctly
        assertTrue("Regions not created correctly", gameBoard2Players.getRegion().length == NUMBER_OF_REGION + 1);
        assertNotSame("Regions should be different", gameBoard2Players.getRegion()[0], gameBoard2Players.getRegion()[1]);
        // Check if cells are created correctly
        assertTrue("Cells not created correctly", gameBoard2Players.getCell().length == NUMBER_OF_CELL);
        assertNotSame("Cells should be different", gameBoard2Players.getCell()[0], gameBoard2Players.getCell()[1]);
        // Check if players are created correctly
        assertTrue("Players not created correctly", gameBoard2Players.getPlayer().length == 2);
        assertNotSame("Players should be different", gameBoard2Players.getPlayer()[0], gameBoard2Players.getPlayer()[1]);
        // Check if shepherd are created correctly with 2 players
        assertTrue("Shepherd not created correctly", gameBoard2Players.getSizeShepherd() == 4);
        // Check if shepherd are created correctly with 4 players
        assertTrue("Shepherd not created correctly", gameBoard4Players.getSizeShepherd() == 4);
        // Check if all players have the correct shepherd
        for (int i = 0; i < NUMBER_OF_PLAYER; i++) {
            assertNotNull("Player hasn't the correct shepherd", gameBoard2Players.getPlayer()[i].getShepherd().get(0));
            assertNotNull("Player hasn't the correct shepherd", gameBoard2Players.getPlayer()[i].getShepherd().get(1));
            // Check if player has more shepherd than max, in this case 2
            try {
                gameBoard2Players.getPlayer()[i].getShepherd().get(2);
            } catch (IndexOutOfBoundsException e) {
                assertTrue("Exception error", e instanceof IndexOutOfBoundsException);
            }
        }
    }

    /**
     * Test if buyCard method work correctly
     * 
     * @throws NoMoneyException
     */
    @Test
    public void testBuyCard() throws NoMoneyException {
        actualPlayer.setMoney(10);
        actualPlayer.getShepherd().get(0).setCell(chosenCell);
        List<Card> buyableCards = gameBoard2Players.getBuyableCards(actualPlayer, 0);
        // Check if exception for arguments is thrown correctly
        try {
            gameBoard2Players.buyCard(null, buyableCards.get(0));
        } catch (IllegalArgumentException e) {
            assertTrue("Exception did'nt correctly throw", e instanceof IllegalArgumentException);
        }
        try {
            gameBoard2Players.buyCard(actualPlayer, null);
        } catch (IllegalArgumentException e) {
            assertTrue("Exception did'nt correctly throw", e instanceof IllegalArgumentException);
        }
        // Check if buyCard work properly
        assertTrue("Card didn't buy correctly", gameBoard2Players.buyCard(actualPlayer, buyableCards.get(0)));
        // Check if a card could be bought or be buyable two times
        assertFalse("Card didn't buy correctly", gameBoard2Players.buyCard(actualPlayer, buyableCards.get(0)));
        buyableCards = gameBoard2Players.getBuyableCards(actualPlayer, 0);
        assertNotSame("Card shouldn't be bought two times", buyableCards.get(0), actualPlayer.getTerrainCards().get(0));
        // Check if a card could be bought without enough money and
        // NoMoneyException is handled correctly
        actualPlayer.setMoney(0);
        assertFalse("Card couldn't be bought", gameBoard2Players.buyCard(actualPlayer, buyableCards.get(0)));
    }

    /**
     * test of getBuyableCard method
     */
    @Test
    public void testGetBuyableCard() {
        // Check if exception for arguments is thrown correctly
        try {
            gameBoard2Players.getBuyableCards(null, 0);
        } catch (IllegalArgumentException e) {
            assertTrue("Exception didn't correctly throw", e instanceof IllegalArgumentException);
        }
    }

    /**
     * test of moveShepherd method
     */
    @Test
    public void testMoveShepherd() {
        // Check if exception for arguments is thrown correctly
        try {
            gameBoard2Players.moveShepherd(null, 0, 0);
        } catch (IllegalArgumentException e) {
            assertTrue("Exception didn't correctly throw", e instanceof IllegalArgumentException);
        }
        int startMoney = 10;
        Shepherd shepherd = actualPlayer.getShepherd().get(0);
        actualPlayer.setMoney(startMoney);
        shepherd.setCell(chosenCell);
        // Check if movement is performed correctly in a near cell and fences
        // are decreased
        Cell nextCell = gameBoard2Players.getCell()[6];
        assertTrue("Movement of shepherd didn't do correctly", gameBoard2Players.moveShepherd(actualPlayer, 0, 6));
        assertSame("Cell wasn't correctly set", nextCell, shepherd.getCell());
        assertEquals("Money shouldn't be decreased", 10, actualPlayer.getMoney());
        assertEquals("Fences weren't decreased", MAX_FENCES - 1, gameBoard2Players.getFences());
        // Check if state of cells are correctly set
        assertTrue("Cell should be locked", chosenCell.getState() == Cell.LOCK);
        assertTrue("Cell should be occupied now", nextCell.getState() == Cell.OCCUPIED);
        // Check if the shepherd can't return into a previous cell
        assertFalse("The chosen cell should be blocked", gameBoard2Players.moveShepherd(actualPlayer, 0, 0));
        // Check if the money of player decrease if movement is performed to a
        // far cell
        gameBoard2Players.moveShepherd(actualPlayer, 0, 2);
        assertEquals("Money weren't decreased", startMoney - MOVE_SHEPHERD_FAR, actualPlayer.getMoney());
        // Check if NoMoneyException is thrown correctly
        actualPlayer.setMoney(0);
        assertFalse("Exception not correctly handled", gameBoard2Players.moveShepherd(actualPlayer, 0, 4));
        // Check final fence decreasing remaining fences (18)
        actualPlayer.setMoney(20);
        for (int i = 3; i < 24; i++) {
            gameBoard2Players.moveShepherd(actualPlayer, 0, i);
        }
        assertTrue("Cell hasn't final fence", gameBoard2Players.getCell()[22].getState() == Cell.FINAL);
    }

    /**
     * Test of moveSheep method
     */
    @Test
    public void testMoveSheep() {
        // Preparing the context
        Shepherd actualShepherd = actualPlayer.getShepherd().get(0);
        actualShepherd.setCell(chosenCell);
        Region region1 = actualShepherd.getCell().getRegion1();
        Region region2 = actualShepherd.getCell().getRegion2();
        Sheep testSheep = region1.getSheep().get(0);
        // Check if exception for arguments is thrown correctly
        try {
            gameBoard2Players.moveSheep(null, 0, testSheep);
        } catch (IllegalArgumentException e) {
            assertTrue("Exception didn't correctly throw", e instanceof IllegalArgumentException);
        }
        try {
            gameBoard2Players.moveSheep(actualPlayer, 0, null);
        } catch (IllegalArgumentException e) {
            assertTrue("Exception didn't correctly throw", e instanceof IllegalArgumentException);
        }
        try {
            gameBoard2Players.moveSheep(null, 0, null);
        } catch (IllegalArgumentException e) {
            assertTrue("Exception didn't correctly throw", e instanceof IllegalArgumentException);
        }
        // Check if the movement is performed correctly
        assertTrue("moveSheep failed", gameBoard2Players.moveSheep(actualPlayer, 0, testSheep));
        assertEquals("Sheep haven't be moved", 0, region1.getSheep().size());
        assertTrue("Sheep isn't in the new region", region2.getSheep().contains(testSheep));
        // Check if the sheep can be moved back
        assertTrue("moveSheep failed", gameBoard2Players.moveSheep(actualPlayer, 0, testSheep));
        assertEquals("Sheep hasn't been moved", 1, region1.getSheep().size());
        assertFalse("Sheep is in the old region", region2.getSheep().contains(testSheep));
        assertTrue("Sheep isn't in the new region", region1.getSheep().contains(testSheep));
        // Check if a sheep in a region far from the shepherd can't be moved
        Sheep farSheep = gameBoard2Players.getRegion()[10].getSheep().get(0);
        assertFalse("the sheep has been moved", gameBoard2Players.moveSheep(actualPlayer, 0, farSheep));
    }

    /**
     * Test of moveBlackSheep method
     */
    @Test
    public void testMoveBlackSheep() {
        BlackSheep blackSheep = gameBoard2Players.getBlackSheep();
        Region starterRegion = gameBoard2Players.getRegion()[SHEEPSBURG];
        // first move of the black sheep, there are all values
        gameBoard2Players.moveBlackSheep();
        Region actualRegion = blackSheep.getRegion();
        // Check if the movement has been performed
        assertNotSame("Blacksheep hasn't been moved", starterRegion, actualRegion);
        // Check if a new region can be not found
        while (true) {
            starterRegion = blackSheep.getRegion();
            gameBoard2Players.moveBlackSheep();
            actualRegion = blackSheep.getRegion();
            if (starterRegion.equals(actualRegion)) {
                assertTrue("FindNewRegion is always true", true);
                break;
            }
        }
    }

    /**
     * Test for findNewRegion method
     */
    @Test
    public void testFindNewRegion() {
        gameBoard2Players.getBlackSheep().setRegion(null);
        // Check if exception for arguments is thrown correctly
        try {
            gameBoard2Players.moveBlackSheep();
        } catch (IllegalArgumentException e) {
            assertTrue("Exception didn't correctly throw", e instanceof IllegalArgumentException);
        }
    }

    /**
     * Test of giveCard method
     */
    @Test
    public void testGiveCard() {
        // Check if all players don't have terrain card after creation
        for (int i = 0; i < NUMBER_OF_PLAYER; i++) {
            assertEquals("Player should not have card now", 0, gameBoard2Players.getPlayer()[i].getTerrainCards().size());
        }
        // Call giveCard method
        gameBoard2Players.giveCard();
        // Now players should have one terrain card for each
        for (int i = 0; i < NUMBER_OF_PLAYER; i++) {
            assertEquals("Player should not have card now", 1, gameBoard2Players.getPlayer()[i].getTerrainCards().size());
        }
    }

    /**
     * Test for giveMoney method
     */
    @Test
    public void testGiveMoney() {
        // Check if all players have correct money set after creation
        for (int i = 0; i < NUMBER_OF_PLAYER; i++) {
            assertEquals("Player has incorrect money", MAX_MONEY_2_PLAYERS, gameBoard2Players.getPlayer()[i].getMoney());
        }
    }

    /**
     * Test for setShepherd method
     */
    @Test
    public void testSetShepherd() {
        // Check if exception for arguments is thrown correctly
        try {
            gameBoard2Players.setShepherd(null, 0, chosenCell.getId());
        } catch (IllegalArgumentException e) {
            assertTrue("Exception didn't correctly throw", e instanceof IllegalArgumentException);
        }
        // Check if cell is set correctly
        assertTrue("setter failed", gameBoard2Players.setShepherd(actualPlayer, 0, chosenCell.getId()));
        assertTrue("Cell is not occupied", gameBoard2Players.getCell()[0].getState() == Cell.OCCUPIED);
        assertSame("Cell is not correct", actualPlayer.getShepherd().get(0).getCell(), chosenCell);
        // Check if a shepherd can't be move in a lock or occupied or final
        // state cell
        Cell newCell = gameBoard2Players.getCell()[1];
        newCell.setState(Cell.OCCUPIED);
        assertFalse("The shepherd shouldn't be moved in an occupied cell", gameBoard2Players.setShepherd(actualPlayer, 0, newCell.getId()));
        newCell.setState(Cell.LOCK);
        assertFalse("The shepherd shouldn't be moved in an occupied cell", gameBoard2Players.setShepherd(actualPlayer, 0, newCell.getId()));
        newCell.setState(Cell.FINAL);
        assertFalse("The shepherd shouldn't be moved in an occupied cell", gameBoard2Players.setShepherd(actualPlayer, 0, newCell.getId()));
    }

    /**
     * Test for calculatePoints method
     */
    @Test
    public void testCalculatePoints() {
        // Preparing the context
        Shepherd actualShepherd = actualPlayer.getShepherd().get(0);
        actualShepherd.setCell(chosenCell);
        Card terrainCard = gameBoard2Players.getBuyableCards(actualPlayer, 0).get(0);
        actualPlayer.setMoney(10);
        gameBoard2Players.buyCard(actualPlayer, terrainCard);
        // Check if points are correctly calculated: should be 13 because 10
        // money plus 1 terrain*3 region of that terrain with a sheep for each
        gameBoard2Players.calculatePoints();
        assertEquals("Points aren't not correctly calculated", 13, actualPlayer.getPoints());
    }

}
