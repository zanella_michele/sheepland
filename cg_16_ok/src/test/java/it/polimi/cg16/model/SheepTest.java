/**
 * 
 */
package it.polimi.cg16.model;

import static it.polimi.cg16.model.Terrain.PLAIN;
import static org.junit.Assert.assertEquals;
import it.polimi.cg16.model.Region;
import it.polimi.cg16.model.Sheep;

import org.junit.Test;

/**
 * @author Michele
 * 
 */
public class SheepTest {
    /**
     * Test for getId method
     */
    @Test
    public void testGetId() {
        Sheep sheep = new Sheep(1, new Region(1, PLAIN));
        assertEquals("Incorrect id", 1, sheep.getId());
    }

}
