/**
 * 
 */
package it.polimi.cg16.model;

import static it.polimi.cg16.model.Terrain.PLAIN;
import static it.polimi.cg16.model.Terrain.WOOD;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import it.polimi.cg16.model.Card;
import it.polimi.cg16.model.Color;
import it.polimi.cg16.model.Player;
import it.polimi.cg16.model.Shepherd;
import it.polimi.cg16.model.Player.NoMoneyException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * @author Michele
 * 
 */
public class PlayerTest {

    private Player player;
    int money;
    int points;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        player = new Player();
        money = 15;
        points = 40;
    }

    /**
     * Test addTerrainCards and getTerrainCards method
     */
    @Test
    public void testAddTerrainCard() {
        Card terrainCard = new Card(WOOD, 0);

        // add a WOOD terrain card
        player.addTerrainCard(terrainCard);

        // Check if addTerrainCard worked correctly
        assertSame("Terrain card not added", terrainCard, player.getTerrainCards().get(0));
    }

    /**
     * Test addShepherd and getShepherd method
     */
    @Test
    public void testAddShepherd() {
        Shepherd shepherd = new Shepherd(Color.RED);

        // add a new shepherd to a player
        player.addShepherd(shepherd);

        // Check if the shepherd was added correctly
        assertSame("Shepherd isn't correct", shepherd, player.getShepherd().get(0));
    }

    /**
     * test setMoney and getMoney method
     */
    @Test
    public void testSetMoney() {
        // Check if after creation money is set to 0
        assertEquals("Money must be zero after creation", 0, player.getMoney());

        // Set money
        player.setMoney(money);

        // Check if setMoney worked properly
        assertEquals("Money is not set correctly", money, player.getMoney());
    }

    /**
     * Test removeMoney method
     * 
     * @throws NoMoneyException
     */
    @Test
    public void testRemoveMoney() throws NoMoneyException {
        int actualMoney = player.getMoney();
        // Check if after creation money is set to 0 and NoMoneyException return
        // the correct player
        // threw it
        try {
            player.removeMoney(money);
        } catch (NoMoneyException e) {
            Player playerException = e.getPlayer();
            assertSame("Players should be the same", player, playerException);
        }

        player.setMoney(money);

        // remove money from shepherd
        player.removeMoney(money - 2);

        // Check if removeMoney works correctly
        assertEquals("removeMoney doesn't work", actualMoney + 2, player.getMoney());
    }

    /**
     * Test the threw of the NoMoneyException
     * 
     * @throws NoMoneyException
     */
    @Test
    public void testNoMoneyException() throws NoMoneyException {
        exception.expect(NoMoneyException.class);

        // Money = 0 after creation so exception is thrown
        player.removeMoney(money);
    }

    /**
     * Test get and set money methods
     */
    @Test
    public void testSetPoints() {
        player.setPoints(points);

        // Points=40 after set
        assertEquals("Points aren't set correctly", points, player.getPoints());
    }

    /**
     * Test getNumberOfCards method
     */
    @Test
    public void testGetNumberOfCards() {
        Card terrainCard1 = new Card(WOOD, 0);
        Card terrainCard2 = new Card(PLAIN, 0);
        // number of wood terrain cards should be 0 after creation
        assertEquals("number of terrain card wrong", 0, player.getNumberOfCards(WOOD));
        // add a terrain card
        player.addTerrainCard(terrainCard1);
        // Now number of wood terrain cards should be 1
        assertEquals("number of terrain card wrong", 1, player.getNumberOfCards(WOOD));
        // add a terrain card
        player.addTerrainCard(terrainCard2);
        // Now number of wood terrain cards should be 1
        assertEquals("number of terrain card wrong", 1, player.getNumberOfCards(WOOD));
    }

}
