/**
 * 
 */
package it.polimi.cg16.model;

import static org.junit.Assert.assertEquals;
import it.polimi.cg16.model.Card;
import it.polimi.cg16.model.DeckFactory;
import it.polimi.cg16.model.Terrain;

import java.util.Iterator;
import java.util.List;

import org.junit.Test;

/**
 * @author Michele
 * 
 */
public class DeckFactoryTest {

	/**
	 * Test if cost of cards is incremental for the same terrain
	 */
	@Test
	public void testDeckCreator() {
		DeckFactory deckFactory = new DeckFactory();
		List<Card> deck = deckFactory.deckCreator(5);

		Iterator<Card> i = deck.iterator();
		int cost = 0;
		Card card1 = i.next();

		while (i.hasNext()) {
			Card card2 = i.next();
			Terrain terrain1 = card1.getTerrain();
			Terrain terrain2 = card2.getTerrain();
			if (terrain1 == terrain2) {

				assertEquals("Cost should be increased", cost, card1.getCost());
				assertEquals("Cost should be increased", cost + 1,
						card2.getCost());
				cost++;
			} else {
				cost = 0;
			}
			card1 = card2;
		}
	}
}
