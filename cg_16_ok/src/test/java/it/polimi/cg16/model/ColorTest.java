/**
 * 
 */
package it.polimi.cg16.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import it.polimi.cg16.model.Color;

import org.junit.Test;

/**
 * @author Michele
 * 
 */
public class ColorTest {

    /**
     * Test type constants
     */
    @Test
    public void testTypeConstant() {
        int i = 0;
        for (Color c : Color.values()) {
            // Check if typeconstant is correct
            assertEquals("typeConstant incorrect", i, c.getId());
            i++;
        }
    }

    /**
     * Test getName method
     */
    @Test
    public void testGetName() {
        String name[] = { "Red", "Blue", "Green", "Yellow" };
        int i = 0;
        for (Color c : Color.values()) {
            // Check if typeName is correct
            assertTrue("typeName incorrect", c.getName().equals(name[i]));
            i++;
        }
    }

}
