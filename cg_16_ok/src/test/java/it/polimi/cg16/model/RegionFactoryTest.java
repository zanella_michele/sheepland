/**
 * 
 */
package it.polimi.cg16.model;

import static it.polimi.cg16.model.GameBoard.NUMBER_OF_REGION;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import it.polimi.cg16.model.Region;
import it.polimi.cg16.model.RegionFactory;

import org.junit.Test;

/**
 * @author Michele
 * 
 */
public class RegionFactoryTest {

    /**
     * Test if all the IDs of the regions created are different
     */
    @Test
    public void testId() {
        RegionFactory regionFactory = new RegionFactory();
        Region[] region = regionFactory.regionCreator();

        // Pass all the region created and verify if different regions have the
        // same id
        for (int i = 0; i < NUMBER_OF_REGION; i++) {
            for (int j = 0; j < NUMBER_OF_REGION; j++) {
                if (i == j) {
                    assertEquals("Id should be the same", region[i], region[j]);
                } else {
                    assertFalse("Id should be different if regions are different", region[i] == region[j]);
                }
            }
        }
    }

    /**
     * Test if all the regions created have one sheep
     */
    @Test
    public void testAddedSheep() {
        RegionFactory regionFactory = new RegionFactory();
        Region[] region = regionFactory.regionCreator();

        for (int i = 1; i < NUMBER_OF_REGION; i++) {
            assertEquals("Sheep not added correctly", 1, region[i].getSheep().size());
        }
    }

}
