/**
 * 
 */
package it.polimi.cg16.model;

import static org.junit.Assert.assertTrue;
import it.polimi.cg16.model.Dice;

import org.junit.Test;

/**
 * @author Andrea
 * 
 */
public class DiceTest {

    @Test
    public void testDice() {
        Dice dice = new Dice();
        int value = dice.roll();
        assertTrue((value <= 6) || (value >= 0));
    }

}
