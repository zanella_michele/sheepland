/**
 * 
 */
package it.polimi.cg16.model;

import static it.polimi.cg16.model.Terrain.PLAIN;
import static it.polimi.cg16.model.Terrain.WOOD;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import it.polimi.cg16.model.Card;
import it.polimi.cg16.model.Terrain;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Michele
 * 
 */
public class CardTest {
    private Card card;
    private Terrain terrain = PLAIN;

    @Before
    public void setUp() {
        card = new Card(terrain, 0);
    }

    /**
     * Test if the constructor works properly
     */
    @Test
    public void testCard() {
        assertSame("Terrain in the card must be PLAIN", terrain, card.getTerrain());
        assertEquals("Cost in the card must be 0", 0, card.getCost());
    }

    @Test
    public void testGetTerrain() {
        Card card2 = new Card(WOOD, 1);

        assertSame("Terrain must be WOOD", WOOD, card2.getTerrain());

        // Terrain must be different
        assertNotSame("Terrain must be different", card.getTerrain(), card2.getTerrain());
    }

    @Test
    public void testGetCost() {
        final int COST = 1;
        Card card2 = new Card(WOOD, COST);

        assertEquals("Cost must be 1", COST, card2.getCost());

        // Cost must be different
        assertFalse("Cost must be different", card.getCost() == card2.getCost());
    }

}
