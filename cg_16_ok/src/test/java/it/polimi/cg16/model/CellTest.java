/**
 * 
 */
package it.polimi.cg16.model;

import static it.polimi.cg16.model.Cell.FREE;
import static it.polimi.cg16.model.Cell.LOCK;
import static it.polimi.cg16.model.Cell.OCCUPIED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import it.polimi.cg16.model.Cell;
import it.polimi.cg16.model.CellFactory;
import it.polimi.cg16.model.Region;
import it.polimi.cg16.model.RegionFactory;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Andrea
 * 
 */
public class CellTest {

    private RegionFactory regionFactory;
    private Region[] region;
    private CellFactory cellFactory;
    private Cell[] cell;
    private List<Cell> nearCells;

    @Before
    public void setUp() {
        // Initialization of region
        regionFactory = new RegionFactory();
        region = regionFactory.regionCreator();
        // Initialization of cell
        cellFactory = new CellFactory();
        cell = cellFactory.cellCreator(region);

    }

    /**
     * Test if cell1 is created correctly
     */
    @Test
    public void testCell1() {

        // Check if constructor of Cell work correctly
        assertNotNull("cell should be instantiate here", cell[1]);

        // Check if cell is created correctly
        assertEquals("Region 1 should be", region[11], cell[1].getRegion1());
        assertEquals("Region 2 should be", region[12], cell[1].getRegion2());
        assertEquals("Value should be", 3, cell[1].getValue());
        assertEquals("State should be", FREE, cell[1].getState());
        assertEquals("Id should be", 1, cell[1].getId());
    }

    /**
     * Test state of cell
     */
    @Test
    public void testState() {
        cell[1].setState(FREE);

        // Check if state is FREE after creation
        assertEquals("State should be FREE", FREE, cell[1].getState());

        // Set state to OCCUPIED
        cell[1].setState(Cell.OCCUPIED);
        // Check if state is OCCUPIED after set
        assertEquals("State should be OCCUPIED", OCCUPIED, cell[1].getState());

        // Set state to LOCK
        cell[1].setState(Cell.LOCK);
        // Check if state is OCCUPIED after set
        assertEquals("State should be LOCK", LOCK, cell[1].getState());

    }

    /**
     * 
     */
    @Test
    public void testNear1() {
        nearCells = cell[1].getNearCells(cell, region);

        assertTrue("nearCells should contain cell[3]", nearCells.contains(cell[3]));
        assertTrue("nearCells should contain cell[4]", nearCells.contains(cell[4]));

    }

    /**
     * 
     */
    @Test
    public void testNear2() {
        nearCells = cell[22].getNearCells(cell, region);

        assertTrue("nearCells should contain cell[24]", nearCells.contains(cell[24]));
        assertTrue("nearCells should contain cell[23]", nearCells.contains(cell[23]));

    }

    /**
     * 
     */
    @Test
    public void testNear3() {
        nearCells = cell[20].getNearCells(cell, region);

        assertTrue("nearCells should contain cell[15]", nearCells.contains(cell[15]));
        assertTrue("nearCells should contain cell[16]", nearCells.contains(cell[16]));
        assertTrue("nearCells should contain cell[27]", nearCells.contains(cell[27]));
        assertTrue("nearCells should contain cell[28]", nearCells.contains(cell[28]));

    }
}
