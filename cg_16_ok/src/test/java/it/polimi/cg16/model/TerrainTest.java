/**
 * 
 */
package it.polimi.cg16.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import it.polimi.cg16.model.Terrain;

import org.junit.Test;

/**
 * @author Andrea
 * 
 */
public class TerrainTest {

    /**
     * Test type constants
     */
    @Test
    public void testTypeConstant() {
        int i = 0;
        for (Terrain t : Terrain.values()) {
            // Check if typeconstant is correct
            assertEquals("typeConstant incorrect", i, t.getTypeConstant());
            i++;
        }
    }

    /**
     * Test getTypeName method
     */
    @Test
    public void testGetTypeName() {
        String name[] = { "Plain", "Wood", "Swamp", "Country", "Mountain", "Desert" };
        int i = 0;
        for (Terrain t : Terrain.values()) {
            // Check if typeName is correct
            assertTrue("typeName incorrect", t.getTypeName().equals(name[i]));
            i++;
        }
    }

}
