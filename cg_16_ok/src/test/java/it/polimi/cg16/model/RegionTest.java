/**
 * 
 */
package it.polimi.cg16.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import it.polimi.cg16.model.Region;
import it.polimi.cg16.model.Sheep;
import it.polimi.cg16.model.Terrain;

import org.junit.Test;

/**
 * @author Andrea
 * 
 */
public class RegionTest {
    private Region sheepsburg = new Region(0);
    private Region region1 = new Region(1, Terrain.PLAIN);
    private Region region2 = new Region(2, Terrain.MOUNTAIN);

    /**
     * Test creation of sheepsburg
     */
    @Test
    public void testSheepsburg() {

        // Check if constructor of Region work correctly
        assertNotNull("sheepsburg should be instantiate here", sheepsburg);

        // Check if region is created correctly
        assertEquals("Id should be", 0, sheepsburg.getId());
        assertNull("Terrain should be null", sheepsburg.getTerrain());
    }

    /**
     * Test creation of region1
     */
    @Test
    public void testRegion1() {

        // Check if constructor of Region work correctly
        assertNotNull("region1 should be instantiate here", region1);

        // Check if region is created correctly
        assertEquals("Id should be", 1, region1.getId());
        assertEquals("Terrain should be PLAIN", Terrain.PLAIN, region1.getTerrain());
    }

    /**
     * Test creation of region2
     */
    @Test
    public void testRegion2() {

        // Check if constructor of Region work correctly
        assertNotNull("region2 should be instantiate here", region2);

        // Check if region is created correctly
        assertEquals("Id should be", 2, region2.getId());
        assertEquals("Terrain should be MOUNTAIN", Terrain.MOUNTAIN, region2.getTerrain());
    }

    /**
     * Test use of various method on sheep
     */
    @Test
    public void testSheep() {
        Sheep sheep = new Sheep(1, region1);
        Sheep sheep2 = new Sheep(2, region1);
        region1.addSheep(sheep);
        // Check if sheep is set correctly
        assertSame("Sheep isn't correct", sheep, region1.getSheep().get(0));
        region1.addSheep(sheep2);
        // Check if sheep is set correctly
        assertEquals("Sheep isn't correct", sheep2, region1.getSheep().get(1));
        region1.removeSheep(sheep);
        // Check if sheep is remove correctly
        assertNotSame("Sheep in region should be null", sheep, region1.getSheep().get(0));
    }

}
