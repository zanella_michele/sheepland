/**
 * 
 */
package it.polimi.cg16.client;

import it.polimi.cg16.client.gui.BlackSheepIcon;
import it.polimi.cg16.client.gui.CardButton;
import it.polimi.cg16.client.gui.CellLabel;
import it.polimi.cg16.client.gui.MoveLabel;
import it.polimi.cg16.client.gui.SheepIcon;
import it.polimi.cg16.client.gui.ShepherdIcon;
import it.polimi.cg16.model.Region;
import it.polimi.cg16.model.Sheep;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;

/**
 * @author Michele
 * 
 */
public class MyActionListener implements ActionListener, MouseListener {
    private static final int CLOSING_NOTICE = -1;
    private NetAdapter netAdapter;

    public MyActionListener(NetAdapter netAdatper) {
        this.netAdapter = netAdatper;
    }

    /**
     * 
     * 
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     **/
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof CardButton) {
            CardButton source = (CardButton) e.getSource();
            if (source.isEnabled()) {
                netAdapter.sendInt(source.getTerrain().getTypeConstant());
            }
        }
    }

    /**
     * Send a closing notice to the server when a client is closing
     */
    public void closingClient() {
        netAdapter.sendInt(CLOSING_NOTICE);
    }

    private int chooseRandomSheep(Region sourceRegion) {
        Random randomSheep = new Random();
        int randomSheepChosen;
        Sheep sheepChosen;
        while (true) {
            randomSheepChosen = randomSheep.nextInt(sourceRegion.getSheep().size());
            sheepChosen = sourceRegion.getSheep().get(randomSheepChosen);
            if (sheepChosen.getId() != 0) {
                break;
            }
        }
        return sheepChosen.getId();
    }

    /**
     * @see java.awt.event.ActionListener#mouseClicked(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() instanceof MoveLabel) {
            MoveLabel source = (MoveLabel) e.getSource();
            if (source.isEnabled()) {
                netAdapter.sendInt(source.getMove().getType());
            }
        } else if (e.getSource() instanceof CellLabel) {
            CellLabel source = (CellLabel) e.getSource();
            if (source.isEnabled()) {
                netAdapter.sendInt(source.getId());
            }
        } else if (e.getSource() instanceof ShepherdIcon) {
            ShepherdIcon source = (ShepherdIcon) e.getSource();
            if (source.isEnabled()) {
                netAdapter.sendInt(source.getShepherdNumber());
            }
        } else if (e.getSource() instanceof BlackSheepIcon) {
            BlackSheepIcon source = (BlackSheepIcon) e.getSource();
            if (source.isEnabled()) {
                netAdapter.sendInt(0);
            }
        } else if (e.getSource() instanceof SheepIcon) {
            SheepIcon source = (SheepIcon) e.getSource();
            if (source.isSelectable()) {
                netAdapter.sendInt(chooseRandomSheep(source.getRegion()));
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseExited(MouseEvent arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mousePressed(MouseEvent arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseReleased(MouseEvent arg0) {
        // TODO Auto-generated method stub

    }
}
