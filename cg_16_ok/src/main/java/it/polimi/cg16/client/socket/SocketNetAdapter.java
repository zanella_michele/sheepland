/**
 * 
 */
package it.polimi.cg16.client.socket;

import it.polimi.cg16.client.NetAdapter;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Michele
 * 
 */
public class SocketNetAdapter implements NetAdapter {
    private static final Logger LOGGER = Logger.getGlobal();
    private static final String CONTEXT = "context";

    private PrintStream ps = System.out;

    private String username;

    private Socket requestSocket;
    private ObjectOutputStream out;
    private ObjectInputStream in;

    public SocketNetAdapter(String username) {
        this.username = username;
    }

    /**
     * Performs the connection with socket
     */
    @Override
    public void connect() {
        try {
            // 1. creating a socket to connect to the server
            requestSocket = new Socket("localhost", 2004);
            ps.println("Connected to localhost in port 2004");
            // 2. get Input and Output streams
            out = new ObjectOutputStream(requestSocket.getOutputStream());
            in = new ObjectInputStream(requestSocket.getInputStream());
            sendMessage(username);
            out.flush();
        } catch (UnknownHostException unknownHost) {
            ps.println("You are trying to connect to an unknown host!");
            LOGGER.log(Level.FINE, CONTEXT, unknownHost);
        } catch (IOException ioException) {
            LOGGER.log(Level.FINE, CONTEXT, ioException);
        }
    }

    /**
     * @see it.polimi.cg16.client.NetAdapter#getUsername()
     */
    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void closeConnection() {
        try {
            in.close();
            out.close();
            requestSocket.close();
        } catch (IOException ioException) {
            LOGGER.log(Level.FINE, CONTEXT, ioException);
        }
    }

    /**
     * 
     * 
     * @see it.polimi.cg16.client.NetAdapter#readInt()
     **/
    @Override
    public int readInt() {
        int i = 0;
        try {
            i = in.readInt();
            // System.out.println(i);
        } catch (IOException e) {
            LOGGER.log(Level.FINE, CONTEXT, e);
        }
        return i;
    }

    /**
     * 
     * 
     * @see it.polimi.cg16.client.NetAdapter#readString()
     **/
    @Override
    public String readString() {
        String s = null;
        try {
            s = (String) in.readObject();
        } catch (ClassNotFoundException e) {
            LOGGER.log(Level.FINE, CONTEXT, e);
        } catch (IOException e) {
            LOGGER.log(Level.FINE, CONTEXT, e);
        }
        return s;
    }

    /**
     * 
     * 
     * @see it.polimi.cg16.client.NetAdapter#sendInt()
     **/
    @Override
    public void sendInt(int num) {
        try {
            out.reset();
            out.writeInt(num);
            out.flush();
            // System.out.println("client>" + num);
        } catch (IOException ioException) {
            LOGGER.log(Level.FINE, CONTEXT, ioException);
        }
    }

    /**
     * 
     * 
     * @see it.polimi.cg16.client.NetAdapter#sendMessage()
     **/
    @Override
    public void sendMessage(String msg) {
        try {
            out.reset();

            out.writeObject(msg);
            out.flush();
            ps.println("client>" + msg);
        } catch (IOException ioException) {
            LOGGER.log(Level.FINE, CONTEXT, ioException);
        }
    }

}
