/**
 * 
 */
package it.polimi.cg16.client;

/**
 * @author Michele
 * 
 */
public interface NetAdapter {
    /**
     * Performs the connection with the specific protocol
     */
    void connect();

    /**
     * Close the connection
     */
    void closeConnection();

    /**
     * Read an integer from the connection
     * 
     * @return
     */
    int readInt();

    /**
     * Read a String from the connection
     * 
     * @return
     */
    String readString();

    /**
     * Send an integer using connection
     * 
     * @param num
     */
    void sendInt(int num);

    /**
     * Send a String using connection
     * 
     * @param msg
     */
    void sendMessage(String msg);

    /**
     * 
     * @return the player username
     */
    String getUsername();
}
