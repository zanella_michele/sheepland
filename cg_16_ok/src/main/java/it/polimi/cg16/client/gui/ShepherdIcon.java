/**
 * 
 */
package it.polimi.cg16.client.gui;

import it.polimi.cg16.client.ShepherdClient;

import javax.swing.ImageIcon;

/**
 * @author Michele
 * 
 */
public class ShepherdIcon extends MovableObject {
    static final int SHEPHERD_ICON_DIMENSION_X = 29;
    static final int SHEPHERD_ICON_DIMENSION_Y = 29;
    /**
     * The connected shepherd
     */
    ShepherdClient shepherd;

    public ShepherdIcon(ShepherdClient shepherd) {
        this.shepherd = shepherd;
        setSize(SHEPHERD_ICON_DIMENSION_X, SHEPHERD_ICON_DIMENSION_Y);
        setOpaque(false);
        setIcon(new ImageIcon("images\\shepherdIcon" + shepherd.getColor().getName() + "Active.png"));
        setDisabledIcon(new ImageIcon("images\\shepherdIcon" + shepherd.getColor().getName() + ".png"));
        setEnabled(false);
        // setBorderPainted(false);
        repaint();
    }

    /**
     * Get the cell of the connected shepherd
     * 
     * @return
     */
    public int getCell() {
        return shepherd.getCell().getId();
    }

    /**
     * Get the id of the connected shepherd
     * 
     * @return the id of the shepherd
     */
    public int getShepherdNumber() {
        return shepherd.getId();
    }
}
