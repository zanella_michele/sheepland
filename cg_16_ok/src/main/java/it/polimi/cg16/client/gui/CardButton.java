/**
 * 
 */
package it.polimi.cg16.client.gui;

import it.polimi.cg16.client.Card;
import it.polimi.cg16.model.Terrain;

import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 * @author Michele
 * 
 */
public class CardButton extends JButton {
    static final int BUTTON_SIZE = 125;

    private Dimension size = new Dimension(BUTTON_SIZE, BUTTON_SIZE);
    private Card card;

    public CardButton() {
        super();
        setPreferredSize(size);
        setOpaque(false);
        setBorderPainted(false);
        setEnabled(false);
        repaint();
    }

    /**
     * Set the connected card to the button
     * 
     * @param card
     */
    public void setCard(Card card) {
        this.card = card;
        ImageIcon cardImage = new ImageIcon("images\\" + card.getTerrain().getTypeName() + card.getCost() + ".png");
        setIcon(cardImage);
        setDisabledIcon(cardImage);
        repaint();
    }

    /**
     * 
     * @return the terrain of the connected card
     */
    public Terrain getTerrain() {
        return card.getTerrain();
    }

    /**
     * 
     * @return the cost of the connected card
     */
    public int getCost() {
        return card.getCost();
    }
}
