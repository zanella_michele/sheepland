/**
 * 
 */
package it.polimi.cg16.client.gui;

import javax.swing.ImageIcon;

/**
 * @author Michele
 * 
 */
public class WolfIcon extends MovableObject {

    public WolfIcon() {
        setEnabled(false);
        setOpaque(false);
        setIcon(new ImageIcon("images\\wolf.png"));
        setDisabledIcon(new ImageIcon("images\\wolf.png"));
        setVisible(true);
        repaint();
    }

}
