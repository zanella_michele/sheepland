/**
 * 
 */
package it.polimi.cg16.client.gui;

import it.polimi.cg16.model.Region;
import it.polimi.cg16.model.Sheep;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * @author Michele
 * 
 */
public class SheepIcon extends MovableObject {
    private int numberOfSheep;
    private Region region;
    private boolean selectable;

    public SheepIcon() {
    }

    public SheepIcon(Region region) {
        this.region = region;
        numberOfSheep = region.getSheep().size();
        selectable = false;
        setEnabled(true);
        setOpaque(false);
        setIcon(new ImageIcon("images\\sheep.png"));
        setDisabledIcon(new ImageIcon("images\\sheep.png"));
    }

    /**
     * This method when called update the text of the label with the current
     * number of sheep of the linked region
     */
    public void updateNumberOfSheep() {
        numberOfSheep = region.getSheep().size();
        for (Sheep sheep : region.getSheep()) {
            if (sheep.getId() == 0) {
                numberOfSheep = region.getSheep().size() - 1;
                break;
            }
        }
        if (numberOfSheep == 0) {
            setVisible(false);
        } else {
            setVisible(true);

        }
        setIcon(new ImageIcon("images\\sheep.png"));
        setDisabledIcon(new ImageIcon("images\\sheep.png"));
        repaint();
        setText("" + numberOfSheep);
        setForeground(Color.BLACK);
        setHorizontalTextPosition(JLabel.CENTER);

    }

    /**
     * 
     * @return the number of sheep in the linked region
     */
    public int getNumberOfSheep() {
        return numberOfSheep;
    }

    /**
     * 
     * @return the linked region
     */
    public Region getRegion() {
        return region;
    }

    /**
     * Set if this label can be selected and so make an action
     * 
     * @param selectable
     */
    public void setSelectable(boolean selectable) {
        this.selectable = selectable;
    }

    /**
     * 
     * @return true if this label is selectable, false otherwise
     */
    public boolean isSelectable() {
        return selectable;
    }
}
