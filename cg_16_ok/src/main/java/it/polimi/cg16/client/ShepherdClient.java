/**
 * 
 */
package it.polimi.cg16.client;

import it.polimi.cg16.model.Color;

/**
 * @author Andrea
 * 
 */
public class ShepherdClient {

    private int id;
    private CellClient cell;
    private Color color;

    public ShepherdClient(CellClient cell, Color color) {
        this.cell = cell;
        this.color = color;
    }

    public ShepherdClient(int id, CellClient cell, Color color) {
        this.id = id;
        this.cell = cell;
        this.color = color;
    }

    /**
     * @return the cell
     */
    public CellClient getCell() {
        return cell;
    }

    /**
     * @param cell
     *            the cell to set
     */
    public void setCell(CellClient cell) {
        this.cell = cell;
    }

    /**
     * 
     * @param the
     *            color of the shepherd
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * 
     * @return the color of the shepherd
     */
    public Color getColor() {
        return color;
    }

    /**
     * 
     * @return the id of the shepherd
     */
    public int getId() {
        return id;
    }

}
