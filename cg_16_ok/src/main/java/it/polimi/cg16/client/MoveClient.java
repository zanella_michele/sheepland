/**
 * 
 */
package it.polimi.cg16.client;

/**
 * @author Michele
 * 
 */
public enum MoveClient {
    SHEEP(0, "Move Sheep"), SHEPHERD(1, "Move Shepherd"), BUY(2, "Buy a terrain card"), KILL(3, "Kill a sheep"), COUPLING(4, "Generate new sheep");

    private int type;
    private String description;

    private MoveClient(int type, String description) {
        this.type = type;
        this.description = description;
    }

    /**
     * 
     * @return a string description of the move
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @return a int type of the move
     */
    public int getType() {
        return type;
    }

}
