/**
 * 
 */
package it.polimi.cg16.client.gui;

import it.polimi.cg16.client.CellClient;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * @author Michele
 * 
 */
public class CellLabel extends JLabel {
    CellClient cell;

    public CellLabel(CellClient cell) {
        this.cell = cell;
        setIcon(new ImageIcon("images\\freeCell.png"));
        setDisabledIcon(new ImageIcon("images\\disabledCell.png"));
        setEnabled(false);
        setOpaque(false);
        setVisible(false);
        // setContentAreaFilled(false);
        // setBorderPainted(false);

    }

    public int getId() {
        return cell.getId();
    }

    /**
     * Update the cellLabel icon according to the state of the cell.
     */
    public void update() {
        if (cell.getState() == CellClient.FREE) {
            setIcon(new ImageIcon("images\\freeCell.png"));
            setDisabledIcon(new ImageIcon("images\\disabledCell.png"));
            repaint();
        } else if (cell.getState() == CellClient.LOCK) {
            setIcon(new ImageIcon("images\\fenceicon.png"));
            setDisabledIcon(new ImageIcon("images\\fenceicon.png"));
            repaint();
        } else if (cell.getState() == CellClient.FINAL) {
            setIcon(new ImageIcon("images\\finalfenceicon.png"));
            setDisabledIcon(new ImageIcon("images\\finalfenceicon.png"));
            repaint();
        }
    }

    /**
     * 
     * @return true if the label should be invisible to avoid interaction with
     *         the shepherd icon when is not enabled
     */
    public boolean isTransparent() {
        return cell.getState() == CellClient.FREE || cell.getState() == CellClient.OCCUPIED;
    }

    /**
     * 
     * @return true if the label is lock or has the final fence
     */
    public boolean isLocked() {
        return cell.getState() == CellClient.LOCK || cell.getState() == CellClient.FINAL;
    }
}
