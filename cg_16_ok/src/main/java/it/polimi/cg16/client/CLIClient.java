/**
 * 
 */
package it.polimi.cg16.client;

import it.polimi.cg16.client.rmi.RMINetAdapter;
import it.polimi.cg16.client.socket.SocketNetAdapter;

import java.io.PrintStream;
import java.util.Scanner;

/**
 * @author Michele
 * 
 */
public class CLIClient {
    static final int SOCKET = 1;
    static final int RMI = 2;

    private PrintStream ps = System.out;

    public CLIClient() {
        Scanner scanner = new Scanner(System.in);
        int netMode;
        String username;
        ps.println("------------------Welcome to Sheepland!----------------");
        ps.println("------Powered by Andrea Sorbelli & Michele Zanella-----");
        ps.println("Please, enter your username:");
        username = scanner.nextLine();
        ps.println("1. Socket");
        ps.println("2. RMI");
        netMode = 0;
        while (netMode > 2 || netMode < 1) {
            netMode = Integer.parseInt(scanner.nextLine());
        }
        if (netMode == SOCKET) {
            NetAdapter socketAdapter = new SocketNetAdapter(username);
            socketAdapter.connect();
            CLIClientController socketClient = new CLIClientController(socketAdapter);
            socketClient.run();
        } else {
            NetAdapter rmiAdapter = new RMINetAdapter(username);
            rmiAdapter.connect();
            CLIClientController rmiClient = new CLIClientController(rmiAdapter);
            rmiClient.run();
        }
    }

    /**
     * Start the client
     * 
     * @param args
     */
    public static void main(String[] args) {
        CLIClient client = new CLIClient();
    }
}
