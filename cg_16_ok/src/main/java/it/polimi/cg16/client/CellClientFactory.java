/**
 * 
 */
package it.polimi.cg16.client;

import it.polimi.cg16.model.Region;

/**
 * @author Andrea
 * 
 */
public class CellClientFactory {

    public CellClient[] cellsCreator(Region[] regions) {
        CellClient[] cell;

        // Initialization of cells
        cell = new CellClient[42];
        cell[0] = new CellClient(0, regions[1], regions[11]);
        cell[1] = new CellClient(1, regions[11], regions[12]);
        cell[2] = new CellClient(2, regions[12], regions[14]);
        cell[3] = new CellClient(3, regions[12], regions[13]);
        cell[4] = new CellClient(4, regions[11], regions[13]);
        cell[5] = new CellClient(5, regions[11], regions[10]);
        cell[6] = new CellClient(6, regions[1], regions[10]);
        cell[7] = new CellClient(7, regions[1], regions[2]);
        cell[8] = new CellClient(8, regions[2], regions[10]);
        cell[9] = new CellClient(9, regions[10], regions[13]);
        cell[10] = new CellClient(10, regions[13], regions[14]);
        cell[11] = new CellClient(11, regions[14], regions[15]);
        cell[12] = new CellClient(12, regions[14], regions[16]);
        cell[13] = new CellClient(13, regions[13], regions[16]);
        cell[14] = new CellClient(14, regions[13], regions[0]);
        cell[15] = new CellClient(15, regions[10], regions[0]);
        cell[16] = new CellClient(16, regions[10], regions[3]);
        cell[17] = new CellClient(17, regions[2], regions[3]);
        cell[18] = new CellClient(18, regions[2], regions[4]);
        cell[19] = new CellClient(19, regions[4], regions[3]);
        cell[20] = new CellClient(20, regions[3], regions[0]);
        cell[21] = new CellClient(21, regions[0], regions[16]);
        cell[22] = new CellClient(22, regions[16], regions[15]);
        cell[23] = new CellClient(23, regions[15], regions[17]);
        cell[24] = new CellClient(24, regions[17], regions[16]);
        cell[25] = new CellClient(25, regions[16], regions[9]);
        cell[26] = new CellClient(26, regions[0], regions[9]);
        cell[27] = new CellClient(27, regions[0], regions[6]);
        cell[28] = new CellClient(28, regions[3], regions[6]);
        cell[29] = new CellClient(29, regions[3], regions[5]);
        cell[30] = new CellClient(30, regions[4], regions[5]);
        cell[31] = new CellClient(31, regions[5], regions[6]);
        cell[32] = new CellClient(32, regions[6], regions[9]);
        cell[33] = new CellClient(33, regions[9], regions[17]);
        cell[34] = new CellClient(34, regions[17], regions[18]);
        cell[35] = new CellClient(35, regions[18], regions[9]);
        cell[36] = new CellClient(36, regions[9], regions[8]);
        cell[37] = new CellClient(37, regions[6], regions[8]);
        cell[38] = new CellClient(38, regions[6], regions[7]);
        cell[39] = new CellClient(39, regions[5], regions[7]);
        cell[40] = new CellClient(40, regions[7], regions[8]);
        cell[41] = new CellClient(41, regions[8], regions[18]);

        return cell;

    }

}
