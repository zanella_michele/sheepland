/**
 * 
 */
package it.polimi.cg16.client;

import it.polimi.cg16.model.Terrain;

/**
 * @author Andrea
 * 
 */
public class Card {

    private Terrain terrain;
    private int cost;

    public Card(Terrain terrain, int cost) {
        this.terrain = terrain;
        this.cost = cost;
    }

    public Terrain getTerrain() {
        return terrain;
    }

    public int getCost() {
        return cost;
    }

}
