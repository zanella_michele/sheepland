/**
 * 
 */
package it.polimi.cg16.client.gui;

import it.polimi.cg16.client.MoveClient;
import it.polimi.cg16.model.Color;

import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 * Class that implement a specific move button.
 * 
 * @see JButton
 * @author Michele
 * 
 */
public class MoveLabel extends JLabel {
    static final int BUTTON_SIZE = 80;

    private MoveClient move;
    private Dimension size = new Dimension(BUTTON_SIZE, BUTTON_SIZE);

    public MoveLabel(MoveClient move) {
        super();
        ImageIcon moveImg = new ImageIcon("images\\" + move.getDescription() + ".png");
        setIcon(moveImg);
        setPreferredSize(size);
        setOpaque(false);
        setEnabled(false);

        repaint();

        this.move = move;
    }

    /**
     * 
     * @return the move assigned to the button
     */
    public MoveClient getMove() {
        return move;
    }

    /**
     * Set the move shepherd button with an icon of the shepherd's color
     * 
     * @param color
     *            of the choosen shepherd
     */
    public void setShepherdColor(Color color) {
        ImageIcon moveImg = new ImageIcon("images\\" + MoveClient.SHEPHERD.getDescription() + color.getName() + ".png");
        setIcon(moveImg);
    }
}
