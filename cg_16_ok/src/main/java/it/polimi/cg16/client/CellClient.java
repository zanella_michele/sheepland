/**
 * 
 */
package it.polimi.cg16.client;

import it.polimi.cg16.model.Region;

/**
 * @author Andrea
 * 
 */
public class CellClient {

    // State constant
    public static final short FREE = 0;
    public static final short LOCK = 1;
    public static final short OCCUPIED = 2;
    public static final short FINAL = 3;

    private int id;
    private Region region1;
    private Region region2;
    private short state;

    /**
     * 
     * @param region1
     *            first adjacent region
     * @param region2
     *            second adjacent region
     */
    public CellClient(int id, Region region1, Region region2) {
        this.id = id;
        this.region1 = region1;
        this.region2 = region2;
        this.state = FREE;
    }

    public void setState(short state) {
        this.state = state;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the state
     */
    public short getState() {
        return state;
    }

    /**
     * @return the region1
     */
    public Region getRegion1() {
        return region1;
    }

    /**
     * @param region1
     *            the region1 to set
     */
    public void setRegion1(Region region1) {
        this.region1 = region1;
    }

    /**
     * @return the region2
     */
    public Region getRegion2() {
        return region2;
    }

    /**
     * @param region2
     *            the region2 to set
     */
    public void setRegion2(Region region2) {
        this.region2 = region2;
    }

}
