/**
 * 
 */
package it.polimi.cg16.client.gui;

import it.polimi.cg16.model.BlackSheep;

import javax.swing.ImageIcon;

/**
 * @author Michele
 * 
 */
public class BlackSheepIcon extends SheepIcon {
    private BlackSheep blackSheep;

    public BlackSheepIcon(BlackSheep blackSheep) {
        super();
        this.blackSheep = blackSheep;
        setSize(GUIConstants.REGION_LABEL_DIMENSION_X, GUIConstants.REGION_LABEL_DIMENSION_Y);
        setIcon(new ImageIcon("images\\blackSheep.png"));
        setDisabledIcon(new ImageIcon("images\\blackSheep.png"));
        setEnabled(false);
        repaint();
    }

    /**
     * Method to see the kill-flag of the linked blacksheep
     * 
     * @return true if the blackSheep linked is killed
     */
    public boolean isKilled() {
        return blackSheep.isKilled();
    }
}
