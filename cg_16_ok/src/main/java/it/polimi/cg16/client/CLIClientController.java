/**
 * 
 */
package it.polimi.cg16.client;

import it.polimi.cg16.model.Sheep;
import it.polimi.cg16.model.Terrain;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

/**
 * @author Andrea & Michele
 * 
 */
public class CLIClientController extends ClientController {
    private PrintStream ps = System.out;

    public CLIClientController(NetAdapter netAdapter) {
        super(netAdapter);
    }

    public void run() {

        helloMessage();

        do {
            receiveObject();
        } while (!"bye".equals(message));
        close();
    }

    /**
     * Send an hello message
     * 
     * @throws IOException
     */
    public void helloMessage() {
        message = netAdapter.readString();
        ps.println("server>" + message);
        ps.println("Waiting for other players...");
    }

    /**
     * Show messaage dialog with close information and perform closing
     * operations
     */
    @Override
    public void close() {
        ps.println("Closing client...");
        super.close();
    }

    /**
     * @see it.polimi.cg16.client.ClientController#showResultsClient()
     */
    @Override
    public void showResultsClient() {
        super.showResultsClient();
        ps.println("Punteggi:");
        for (Terrain t : Terrain.values()) {
            ps.print("\t" + t.getTypeName());
        }
        ps.println();
        int tot = 0;
        ps.print(player.getUsername() + " (Tu):\t");
        for (Terrain t : Terrain.values()) {
            int num = 0;
            for (Card c : player.getTerrainCards()) {
                if (c.getTerrain() == t) {
                    num++;
                }
            }
            tot = tot + num * sheepsPerTerrain.get(t);
            ps.print(num + "*" + sheepsPerTerrain.get(t) + " +\t");
        }
        tot = tot + player.getMoney();
        ps.println(player.getMoney() + " = " + tot);
        for (PlayerClient p : otherPlayers) {
            tot = 0;
            ps.print(p.getUsername() + " :\t");
            for (Terrain t : Terrain.values()) {
                int num = 0;
                for (Card c : p.getTerrainCards()) {
                    if (c.getTerrain() == t) {
                        num = c.getCost();
                    }
                }
                tot = tot + num * sheepsPerTerrain.get(t);
                ps.print(num + "*" + sheepsPerTerrain.get(t) + " +\t");
            }
            tot = tot + p.getMoney();
            ps.println(p.getMoney() + " = " + tot);

        }

        ps.println(player.getUsername() + "\t(Tu): " + " punti!");

    }

    /**
     * @see it.polimi.cg16.client.ClientController#chooseShepherd()
     */
    @Override
    public void chooseShepherd() {
        Scanner keyboard = new Scanner(System.in);
        ps.println("Scegli il pastore (0 o 1)");
        int cell = Integer.parseInt(keyboard.nextLine());
        netAdapter.sendInt(cell);
    }

    /**
     * it.polimi.cg16.client.ClientController#chooseCellClient()
     */
    @Override
    public void chooseCellClient() {
        Scanner keyboard = new Scanner(System.in);
        ps.println("Scegli la cella tra le seguenti:");
        CellClient[] cells = gameBoard.getCells();
        for (int i = 0; i < cells.length; i++) {
            if (cells[i].getState() == 0) {
                ps.print(cells[i].getId());
                ps.print(" ");
            }
        }
        ps.println();
        int cellId = Integer.parseInt(keyboard.nextLine());
        netAdapter.sendInt(cellId);
    }

    /**
     * it.polimi.cg16.client.ClientController#chooseCardClient()
     */
    @Override
    public void chooseCardClient() {
        int length = netAdapter.readInt();
        int[] terrainConstant = new int[length];
        int[] cost = new int[length];
        Scanner keyboard = new Scanner(System.in);
        ps.println("Scegli una delle seguenti carte:");
        for (int i = 0; i < length; i++) {
            ps.print("Premi " + i + " per ");
            terrainConstant[i] = netAdapter.readInt();
            cost[i] = netAdapter.readInt();
            for (Terrain t : Terrain.values()) {
                if (t.getTypeConstant() == terrainConstant[i]) {
                    ps.print("carta di tipo " + t.getTypeName() + " ");
                }
            }
            ps.print("che costa " + cost[i]);
            ps.println();
        }
        int chosenCard = Integer.parseInt(keyboard.nextLine());
        netAdapter.sendInt(terrainConstant[chosenCard]);
    }

    /**
     * @see it.polimi.cg16.client.ClientController#chooseSheepClient()
     */
    @Override
    public void chooseSheepClient() {
        int chosenShepherd = netAdapter.readInt();
        Scanner keyboard = new Scanner(System.in);
        ps.println("Scegli la pecora tra le seguenti:");
        if (player.getShepherds().get(chosenShepherd).getCell().getRegion1().getId() == 0) {
            ps.println("Sheepsburg");
            for (Sheep s : player.getShepherds().get(chosenShepherd).getCell().getRegion1().getSheep()) {
                ps.println("Pecora " + s.getId());
            }
        } else {
            ps.println("Regione " + player.getShepherds().get(chosenShepherd).getCell().getRegion1().getId() + " di tipo " + player.getShepherds().get(chosenShepherd).getCell().getRegion1().getTerrain().getTypeName());
            for (Sheep s : player.getShepherds().get(chosenShepherd).getCell().getRegion1().getSheep()) {
                ps.println("Pecora " + s.getId());
            }
        }
        if (player.getShepherds().get(chosenShepherd).getCell().getRegion2().getId() == 0) {
            ps.println("Sheepsburg");
            for (Sheep s : player.getShepherds().get(chosenShepherd).getCell().getRegion2().getSheep()) {
                ps.println("Pecora " + s.getId());
            }
        } else {
            ps.println("Regione " + player.getShepherds().get(chosenShepherd).getCell().getRegion2().getId() + " di tipo " + player.getShepherds().get(chosenShepherd).getCell().getRegion2().getTerrain().getTypeName());
            for (Sheep s : player.getShepherds().get(chosenShepherd).getCell().getRegion2().getSheep()) {
                ps.println("Pecora " + s.getId());
            }
        }
        ps.println();
        int idSheep = Integer.parseInt(keyboard.nextLine());

        netAdapter.sendInt(idSheep);
    }

    /**
     * @see it.polimi.cg16.client.ClientController#chooseMoveClient()
     */
    @Override
    public void chooseMoveClient() {
        int length = netAdapter.readInt();
        Scanner keyboard = new Scanner(System.in);
        ps.println("Scegli la mossa tra le seguenti:");

        for (int i = 0; i < length; i++) {
            int moveType = netAdapter.readInt();
            for (MoveClient m : MoveClient.values()) {
                if (m.getType() == moveType) {
                    ps.println("Premi " + m.getType() + " per " + m.getDescription());
                }
            }
        }
        ps.println();
        int chosenMoveType = Integer.parseInt(keyboard.nextLine());
        netAdapter.sendInt(chosenMoveType);
    }
}