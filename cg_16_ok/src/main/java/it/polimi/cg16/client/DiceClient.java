/**
 * 
 */
package it.polimi.cg16.client;

/**
 * @author Michele
 * 
 */
public class DiceClient {
    private int numExtracted;

    /**
     * @return the actual number of the dice
     */
    public int getNumExtracted() {
        return numExtracted;
    }

    /**
     * @param numExtracted
     *            the number extracted by dice in server
     */
    public void setNumExtracted(int numExtracted) {
        this.numExtracted = numExtracted;
    }
}
