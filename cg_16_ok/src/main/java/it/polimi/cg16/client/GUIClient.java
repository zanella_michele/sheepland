/**
 * 
 */
package it.polimi.cg16.client;

import it.polimi.cg16.client.gui.GUIConstants;
import it.polimi.cg16.client.rmi.RMINetAdapter;
import it.polimi.cg16.client.socket.SocketNetAdapter;

import java.io.PrintStream;
import java.util.Scanner;

/**
 * @author Michele
 * 
 */
public class GUIClient {
    static final int SOCKET = 1;
    static final int RMI = 2;

    private PrintStream ps = System.out;

    public GUIClient() {
        new GUIConstants();
        Scanner scanner = new Scanner(System.in);
        int netMode;
        String username;
        ps.println("------------------Welcome to Sheepland!----------------");
        ps.println("------Powered by Andrea Sorbelli & Michele Zanella-----");
        ps.println("Please, enter your username:");
        username = scanner.nextLine();
        ps.println("Select the network connection");
        ps.println("1. Socket");
        ps.println("2. RMI");
        netMode = 0;
        while (netMode > 2 || netMode < 1) {
            netMode = Integer.parseInt(scanner.nextLine());
        }
        if (netMode == SOCKET) {
            NetAdapter socketAdapter = new SocketNetAdapter(username);
            socketAdapter.connect();
            GUIClientController socketClient = new GUIClientController(socketAdapter);
            socketClient.run();
        } else {
            NetAdapter rmiAdapter = new RMINetAdapter(username);
            rmiAdapter.connect();
            GUIClientController rmiClient = new GUIClientController(rmiAdapter);
            rmiClient.run();
        }
    }

    /**
     * Start the client
     * 
     * @param args
     */
    public static void main(String[] args) {
        GUIClient client = new GUIClient();
    }
}
