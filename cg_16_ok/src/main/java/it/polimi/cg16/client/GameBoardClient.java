/**
 * 
 */
package it.polimi.cg16.client;

import it.polimi.cg16.model.BlackSheep;
import it.polimi.cg16.model.Region;
import it.polimi.cg16.model.RegionFactory;
import it.polimi.cg16.model.Sheep;
import it.polimi.cg16.model.Wolf;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrea & Michele
 * 
 */
public class GameBoardClient {
    // Constants
    static final int NUMBER_OF_REGION = 18;
    static final int NUMBER_OF_CELL = 42;
    static final int SHEEPSBURG = 0;
    static final int CARD_PER_TERRAIN = 5;
    static final int STARTER_CARD_FOREACH = 1;

    // Game components
    private List<Card> deck;
    private RegionFactory regionsFactory;
    private Region[] regions;
    private CellClientFactory cellsFactory;
    private CellClient[] cells;
    private List<ShepherdClient> otherShepherds;
    private BlackSheep blackSheep;
    private Wolf wolf;

    public GameBoardClient() {

        // Initialization of decks
        deck = new ArrayList<Card>();

        otherShepherds = new ArrayList<>();

        // Initialization of region
        regionsFactory = new RegionFactory();
        regions = regionsFactory.regionCreator();
        // Initialization of cell
        cellsFactory = new CellClientFactory();
        cells = cellsFactory.cellsCreator(regions);

        // Initialization of Black sheep
        blackSheep = new BlackSheep(0, getRegions()[SHEEPSBURG]);
        regions[0].addSheep(blackSheep);

        // Initialization of wolf
        wolf = new Wolf(getRegions()[SHEEPSBURG]);

    }

    /**
     * Set the sheeps list in a given region
     * 
     * @param idRegion
     * @param sheeps
     *            to set
     */
    public void setRegionsSheeps(int idRegion, List<Sheep> sheeps) {
        regions[idRegion].setSheeps(sheeps);
    }

    /**
     * @return the regions of the gameboard
     */
    public Region[] getRegions() {
        return regions;
    }

    /**
     * @return the cells of the gameboard
     */
    public CellClient[] getCells() {
        return cells;
    }

    /**
     * @return the deckView
     */
    public List<Card> getDeckClient() {
        return deck;
    }

    /**
     * @param deckClient
     *            the deckView to set
     */
    public void setDeckClient(List<Card> deckClient) {
        this.deck = deckClient;
    }

    /**
     * 
     * @return the id of the region in which is the blacksheep
     */
    public int getBlackSheepIdRegion() {
        return blackSheep.getRegion().getId();
    }

    /**
     * 
     * @returnthe id of the region in which is the wolf
     */
    public int getWolfIdRegion() {
        return wolf.getRegion().getId();
    }

    /**
     * Set the blacksheep to the region of given id
     * 
     * @param idRegion
     */
    public void setBlackSheep(int idRegion) {
        blackSheep.setRegion(regions[idRegion]);
    }

    /**
     * Set the wolf to the region of given id
     * 
     * @param idRegion
     */
    public void setWolf(int idRegion) {
        wolf.setRegion(regions[idRegion]);
    }

    /**
     * @return the otherShepherds list
     */
    public List<ShepherdClient> getOtherShepherds() {
        return otherShepherds;
    }

    /**
     * @param otherShepherds
     *            the otherShepherds to set
     */
    public void setOtherShepherds(List<ShepherdClient> otherShepherds) {
        this.otherShepherds = otherShepherds;
    }

    /**
     * 
     * @return the gameboard blacksheep
     */
    public BlackSheep getBlackSheep() {
        return blackSheep;
    }
}
