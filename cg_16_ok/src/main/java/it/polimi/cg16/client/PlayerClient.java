/**
 * 
 */
package it.polimi.cg16.client;

import it.polimi.cg16.model.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Michele
 * 
 */
public class PlayerClient {
    private int money;
    private int chosenShepherd;
    private String username;
    private List<Card> terrainCards;
    private List<ShepherdClient> shepherds;
    private Color color;

    public PlayerClient(String name) {
        terrainCards = new ArrayList<Card>();
        shepherds = new ArrayList<ShepherdClient>();
        money = 0;
        this.username = name;
    }

    /**
     * @return the chosenShepherd
     */
    public int getChosenShepherd() {
        return chosenShepherd;
    }

    /**
     * @param chosenShepherd
     *            the chosenShepherd to set
     */
    public void setChosenShepherd(int chosenShepherd) {
        this.chosenShepherd = chosenShepherd;
    }

    /**
     * @return the money
     */
    public int getMoney() {
        return money;
    }

    /**
     * @param money
     *            the money to set
     */
    public void setMoney(int money) {
        this.money = money;
    }

    /**
     * 
     * @return the player's username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @return the terrainCards
     */
    public List<Card> getTerrainCards() {
        return terrainCards;
    }

    /**
     * @param terrainCards
     *            the terrainCards to set
     */
    public void setTerrainCards(List<Card> terrainCards) {
        this.terrainCards = terrainCards;
    }

    /**
     * @return the shepherds
     */
    public List<ShepherdClient> getShepherds() {
        return shepherds;
    }

    /**
     * @param shepherds
     *            the shepherds to set
     */
    public void setShepherds(List<ShepherdClient> shepherds) {
        this.shepherds = shepherds;
    }

    /**
     * 
     * @param the
     *            color of the player
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * 
     * @return the color of the player
     */
    public Color getColor() {
        return color;
    }

}
