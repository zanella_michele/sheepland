/**
 * 
 */
package it.polimi.cg16.net;

import java.rmi.RemoteException;

/**
 * @author Andrea
 * 
 */
public class RmiCommImpl implements RmiComm {

    String message = null;
    int number = -1;
    String sender = null;
    String reader = null;
    int read = 1;

    /*
     * (non-Javadoc)
     * 
     * @see it.polimi.cg16.net.RmiComm#writeMessage(java.lang.String,
     * java.lang.String)
     */
    public void writeMessage(String message, String sender) throws RemoteException {
        this.message = message;
        this.sender = sender;
        this.read = 0;
    }

    /*
     * (non-Javadoc)
     * 
     * @see it.polimi.cg16.net.RmiComm#readMessage(java.lang.String)
     */
    public String readMessage(String reader) throws RemoteException {
        this.read = 1;
        this.reader = reader;
        return this.message;

    }

    /*
     * (non-Javadoc)
     * 
     * @see it.polimi.cg16.net.RmiComm#writeNumber(int, java.lang.String)
     */
    public void writeNumber(int number, String sender) throws RemoteException {
        this.number = number;
        this.sender = sender;
        this.read = 0;
    }

    /*
     * (non-Javadoc)
     * 
     * @see it.polimi.cg16.net.RmiComm#readNumber(java.lang.String)
     */
    public int readNumber(String reader) throws RemoteException {
        this.read = 1;
        this.reader = reader;
        return this.number;

    }

    /*
     * (non-Javadoc)
     * 
     * @see it.polimi.cg16.net.RmiComm#getRead()
     */
    public int getRead() throws RemoteException {
        return this.read;
    }

    /*
     * (non-Javadoc)
     * 
     * @see it.polimi.cg16.net.RmiComm#getSender()
     */
    public String getSender() throws RemoteException {
        return this.sender;
    }

    /*
     * (non-Javadoc)
     * 
     * @see it.polimi.cg16.net.RmiComm#getReader()
     */
    public String getReader() throws RemoteException {

        return this.reader;
    }
}
