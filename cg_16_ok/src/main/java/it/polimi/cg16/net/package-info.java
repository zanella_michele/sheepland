/**
 * This package contains the <strong>net</strong> part.
 * Contains all connection class used by the server.
 */

package it.polimi.cg16.net;