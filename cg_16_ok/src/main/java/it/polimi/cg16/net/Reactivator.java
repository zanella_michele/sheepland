/**
 * 
 */
package it.polimi.cg16.net;

import it.polimi.cg16.controller.PlayerController;

/**
 * @author Michele
 * 
 */
public class Reactivator implements Runnable {

    private PlayerController player;
    private Adapter adapter;

    @Override
    public void run() {
        player.setActive(true);
        player.setAdapter(adapter);
        player.getGame().initialUpdate();
    }

    /**
     * Reactivate a player
     * 
     * @param player
     *            PlayerController to reactivate
     * @param adapter
     *            to use
     */
    public void reActivatePlayer(PlayerController player, Adapter adapter) {
        this.player = player;
        this.adapter = adapter;
    }

}
