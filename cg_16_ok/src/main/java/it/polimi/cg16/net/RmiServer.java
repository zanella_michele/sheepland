/**
 * 
 */
package it.polimi.cg16.net;

import it.polimi.cg16.controller.Starter;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Andrea
 * 
 */
public class RmiServer implements Runnable {
    private static final Logger LOGGER = Logger.getGlobal();
    private static final String CONTEXT = "context";
    private GameDatabase database;
    private Reactivator reactivator;
    private Starter starter;

    /**
     * Creator of RmiServer
     * 
     * @param database
     * @param reactivator
     * @param starter
     */
    public RmiServer(GameDatabase database, Reactivator reactivator, Starter starter) {
        this.database = database;
        this.reactivator = reactivator;
        this.starter = starter;
    }

    public void run() {

        try {
            RmiConnectionImpl rmiStart = new RmiConnectionImpl(database, reactivator, starter);
            Registry registry = LocateRegistry.createRegistry(1099);
            RmiConnection stub = (RmiConnection) UnicastRemoteObject.exportObject(rmiStart, 1099);
            registry.rebind("RmiStart", stub);
            System.out.println("Rmi Server is ready to listen...");
        } catch (RemoteException e) {
            LOGGER.log(Level.FINE, CONTEXT, e);
        }
    }
}