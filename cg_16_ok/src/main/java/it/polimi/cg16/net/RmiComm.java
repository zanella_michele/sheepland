/**
 * 
 */
package it.polimi.cg16.net;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Andrea
 * 
 */
public interface RmiComm extends Remote {
    /**
     * Write a message to the communication class
     * 
     * @param message
     * @param sender
     * @throws RemoteException
     */
    public void writeMessage(String message, String sender) throws RemoteException;

    /**
     * Read a message from the communication class
     * 
     * @param reader
     * @return
     * @throws RemoteException
     */
    public String readMessage(String reader) throws RemoteException;

    /**
     * Write a number to the communication class
     * 
     * @param number
     * @param sender
     * @throws RemoteException
     */
    public void writeNumber(int number, String sender) throws RemoteException;

    /**
     * Read a number from the communication class
     * 
     * @param reader
     * @return
     * @throws RemoteException
     */
    public int readNumber(String reader) throws RemoteException;

    /**
     * 
     * @return Integer to know if the message has been read or not
     * @throws RemoteException
     */
    public int getRead() throws RemoteException;

    /**
     * 
     * @return the sender of the last message
     * @throws RemoteException
     */
    public String getSender() throws RemoteException;

    public String getReader() throws RemoteException;

}