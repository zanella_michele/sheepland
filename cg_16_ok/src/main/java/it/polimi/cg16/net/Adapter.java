/**
 * 
 */
package it.polimi.cg16.net;

import it.polimi.cg16.controller.Game;
import it.polimi.cg16.exceptions.InactivePlayerException;

/**
 * @author Michele
 * 
 */
public interface Adapter {

    /**
     * Show results and wait for game to be ended
     * 
     * @param game
     */
    void showResults(Game game);

    /**
     * Send a bye message and close the connection
     */
    public void byeMessage();

    /**
     * 
     * @return Integer read from the active connection
     * @throws InactivePlayerException
     */
    int readInt() throws InactivePlayerException;

    /**
     * 
     * @return String read from the active connection
     * @throws InactivePlayerException
     */
    String readString() throws InactivePlayerException;

    /**
     * 
     * @param msg
     *            to send to the active connection
     */
    void sendMessage(String msg);

    /**
     * 
     * @param num
     *            to send to the active connection
     */
    void sendInt(int num);

}
