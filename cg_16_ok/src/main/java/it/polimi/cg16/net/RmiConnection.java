/**
 * 
 */
package it.polimi.cg16.net;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Andrea
 * 
 */
public interface RmiConnection extends Remote {
    /**
     * Enter a game
     * 
     * @param numberOfPlayer
     * @return the number of player
     * @throws RemoteException
     */
    public int enterGame(int numberOfPlayer) throws RemoteException;

    /**
     * Create a new adapter
     * 
     * @return the number of player
     * @throws RemoteException
     */
    public int createAdapter() throws RemoteException;
}
