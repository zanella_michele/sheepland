/**
 * 
 */
package it.polimi.cg16.net;

import static it.polimi.cg16.controller.Server.MAX_PLAYER;
import it.polimi.cg16.controller.PlayerController;
import it.polimi.cg16.controller.Server;
import it.polimi.cg16.controller.Starter;
import it.polimi.cg16.exceptions.InactivePlayerException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Timer;

/**
 * @author Andrea
 * 
 */
public class RmiConnectionImpl implements RmiConnection {
    private static final Logger LOGGER = Logger.getGlobal();

    private List<RmiAdapter> rmiAdapter = new ArrayList<RmiAdapter>();

    private int numberOfPlayer = 0;

    private GameDatabase database;
    private Reactivator reactivator;
    private Starter starter;

    public RmiConnectionImpl(GameDatabase database, Reactivator reactivator, Starter starter) {
        this.database = database;
        this.reactivator = reactivator;
        this.starter = starter;
    }

    /**
 * 
 */
    public int enterGame(int numberOfPlayer) throws RemoteException {
        Timer t1 = new Timer(Server.GAME_START_TIME_OUT, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                starter.startGame();
            }
        });
        String username;
        try {
            username = rmiAdapter.get(numberOfPlayer).readString();
        } catch (InactivePlayerException e1) {
            LOGGER.log(Level.FINE, "context", e1);
            throw new RemoteException();
        }
        System.out.println(username);
        if (database.hasPlayer(username)) {
            PlayerController player = database.getPlayer(username);
            rmiAdapter.get(numberOfPlayer).setPlayerController(player);
            reactivator.reActivatePlayer(player, rmiAdapter.get(numberOfPlayer));
            Thread t = new Thread(reactivator);
            t.start();
            return player.getId();
        } else if (database.isActive(username)) {
            rmiAdapter.get(numberOfPlayer).sendMessage("bye");
        } else {
            PlayerController player = new PlayerController(rmiAdapter.get(numberOfPlayer), numberOfPlayer % MAX_PLAYER, username);
            database.addPlayer(player);
            rmiAdapter.get(numberOfPlayer).setPlayerController(player);
            t1.stop();
        }

        this.numberOfPlayer++;
        if (database.waitPlayerSize() >= Server.MIN_PLAYER) {
            t1.start();
        }
        if (database.waitPlayerSize() == MAX_PLAYER) {
            t1.stop();
            starter.startGame();
            return MAX_PLAYER - 1;

        }
        return (this.numberOfPlayer - 1) % MAX_PLAYER;

    }

    @Override
    public int createAdapter() throws RemoteException {
        RmiAdapter adapter = new RmiAdapter(numberOfPlayer);
        rmiAdapter.add(adapter);
        return numberOfPlayer;
    }
}