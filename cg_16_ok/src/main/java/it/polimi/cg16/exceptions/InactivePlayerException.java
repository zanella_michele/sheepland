/**
 * 
 */
package it.polimi.cg16.exceptions;

import it.polimi.cg16.controller.PlayerController;

/**
 * @author Michele
 * 
 */
public class InactivePlayerException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 661566560058418764L;

    private PlayerController player;

    public void setPlayer(PlayerController player) {
        this.player = player;
    }

    public PlayerController getPlayer() {
        return player;
    }
}
