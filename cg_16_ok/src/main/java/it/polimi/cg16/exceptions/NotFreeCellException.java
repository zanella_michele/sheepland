/**
 * 
 */
package it.polimi.cg16.exceptions;

import it.polimi.cg16.model.Cell;

/**
 * @author Michele
 * 
 */
public class NotFreeCellException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -4128601004750082041L;
    private final Cell cell;

    /**
     * 
     * @param cell
     */
    public NotFreeCellException(Cell cell) {
        this.cell = cell;
    }

    /**
     * 
     */
    public Cell getCell() {
        return cell;
    }

}
