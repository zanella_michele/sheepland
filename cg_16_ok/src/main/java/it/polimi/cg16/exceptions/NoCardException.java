/**
 * 
 */
package it.polimi.cg16.exceptions;

import it.polimi.cg16.model.Terrain;

/**
 * @author Andrea
 * 
 */
public class NoCardException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2010179333634795594L;
	private final Terrain terrain;

	/**
	 * Set terrainType that thrown the exception
	 * 
	 * @param terrainType
	 */

	public NoCardException(Terrain terrain) {
		this.terrain = terrain;
	}

	/**
	 * Get the terrainType that thrown the exception
	 * 
	 * @return
	 */
	public Terrain getTerrainType() {
		return terrain;
	}

}
