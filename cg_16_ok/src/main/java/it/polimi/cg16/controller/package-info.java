/**
 * This package contains the <strong>controller</strong> part.
 * Contains all logic of the game and moves.
 * Contains some utiliy classes that manage the thread game and the server main class.
 */

package it.polimi.cg16.controller;