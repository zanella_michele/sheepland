/**
 * 
 */
package it.polimi.cg16.controller;

import it.polimi.cg16.net.GameDatabase;
import it.polimi.cg16.net.Reactivator;
import it.polimi.cg16.net.RmiServer;
import it.polimi.cg16.net.SocketServer;

/**
 * @author Michele
 * 
 */
public class Server {
    public static final int MAX_PLAYER = 4;
    public static final int MIN_PLAYER = 2;
    public static final int GAME_START_TIME_OUT = 60000;

    Reactivator reactivator;
    GameDatabase database;
    Starter starter;

    public Server() {
        reactivator = new Reactivator();
        database = new GameDatabase();
        starter = new Starter(database);
        SocketServer socketServer = new SocketServer(database, reactivator, starter);
        RmiServer rmiServer = new RmiServer(database, reactivator, starter);

        Thread t1 = new Thread(socketServer);
        Thread t2 = new Thread(rmiServer);

        t1.start();
        t2.start();

    }

    public static void main(String[] args) {
        Server server = new Server();
    }
}
