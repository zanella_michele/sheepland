/**
 * 
 */
package it.polimi.cg16.controller;

/**
 * @author Michele
 * 
 */
public class GameThread extends Thread {
    private Game game;

    public GameThread(Game game) {
        super(game);
        this.game = game;
    }

    /**
     * 
     * @return the game associated with this thread
     */
    public Game getGame() {
        return game;
    }
}
