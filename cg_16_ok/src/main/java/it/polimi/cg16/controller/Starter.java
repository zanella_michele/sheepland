/**
 * 
 */
package it.polimi.cg16.controller;

import it.polimi.cg16.net.GameDatabase;

/**
 * This class implements a starter utility that manages the start game step
 * 
 * @author Michele
 * 
 */
public class Starter {
    private GameDatabase database;

    public Starter(GameDatabase database) {
        this.database = database;
    }

    /**
     * This syncronized method allows to rmi or socket server to start a new
     * game thread with an associated game.
     */
    public synchronized void startGame() {
        if (database.waitPlayerSize() >= Server.MIN_PLAYER) {
            Game game = new Game(database.getWaitPlayers());
            database.updateDatabase();
            GameThread t = new GameThread(game);
            t.start();
            Thread ender = new Thread(new Ender(t, database));
            ender.start();
        }
    }
}
