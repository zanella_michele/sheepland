/**
 * 
 */
package it.polimi.cg16.controller;

import it.polimi.cg16.net.GameDatabase;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class implements an object that wait the given GameThread to terminate
 * and getting its game calls database's removePlayers method
 * 
 * @author Michele
 * 
 */
public class Ender implements Runnable {
    private static final Logger LOGGER = Logger.getGlobal();
    private static final String CONTEXT = "context";

    private GameThread thread;
    private GameDatabase database;

    public Ender(GameThread thread, GameDatabase database) {
        this.thread = thread;
        this.database = database;
    }

    /**
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        Game game = thread.getGame();
        try {
            thread.join();
            database.removePlayers(game);
        } catch (InterruptedException e) {
            LOGGER.log(Level.FINE, CONTEXT, e);
            database.removePlayers(game);
        }
    }

}
