/**
 * 
 */
package it.polimi.cg16.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Michele
 * 
 */
public class Cell implements Serializable {
    // State constant
    static final short FREE = 0;
    static final short LOCK = 1;
    static final short OCCUPIED = 2;
    static final short FINAL = 3;

    private int id;
    private Region region1;
    private Region region2;
    private int value;
    private short state;

    /**
     * 
     * @param region1
     *            first adjacent region
     * @param region2
     *            second adjacent region
     * @param value
     *            dice number to get to pass through the cell
     */
    public Cell(int id, Region region1, Region region2, int value) {
        this.id = id;
        this.region1 = region1;
        this.region2 = region2;
        this.value = value;
        this.state = FREE;
    }

    /**
     * Find the cells near the one given All cell returned are FREE
     * 
     * @param cell
     * @return List<Cell> with all the near cells
     */
    public List<Cell> getNearCells(Cell[] cells, Region[] region) {
        List<Cell> nearCells;
        nearCells = new ArrayList<Cell>();

        for (int i = 0; i < cells.length; i++) {
            if ((this.region1 == cells[i].region1) && (this.id != cells[i].id)) {
                for (int j = 0; j < cells.length; j++) {
                    if (((this.region2 == cells[j].region1) && (cells[i].region2 == cells[j].region2) && (cells[j].id != this.id) && (cells[j].id != cells[i].id)) || ((this.region2 == cells[j].region2) && (cells[i].region2 == cells[j].region1) && (cells[j].id != this.id) && (cells[j].id != cells[i].id))) {
                        if (!(nearCells.contains(cells[i]))) {
                            nearCells.add(cells[i]);
                        }
                        if (!(nearCells.contains(cells[j]))) {
                            nearCells.add(cells[j]);
                        }
                    }
                }
            } else if ((this.region1 == cells[i].region2) && (this.id != cells[i].id)) {
                for (int j = 0; j < cells.length; j++) {
                    if (((this.region2 == cells[j].region1) && (cells[i].region1 == cells[j].region2) && (cells[j].id != this.id) && (cells[j].id != cells[i].id)) || ((this.region2 == cells[j].region2) && (cells[i].region1 == cells[j].region1) && (cells[j].id != this.id) && (cells[j].id != cells[i].id))) {
                        if (!nearCells.contains(cells[i])) {
                            nearCells.add(cells[i]);
                        }
                        if (!nearCells.contains(cells[j])) {
                            nearCells.add(cells[j]);
                        }
                    }
                }

            }
        }
        return nearCells;
    }

    /**
     * @return id of the cell
     */
    public int getId() {
        return id;
    }

    /**
     * @return region1 first adjacent region
     */
    public Region getRegion1() {
        return region1;
    }

    /**
     * @return region2 second adjacent region
     */
    public Region getRegion2() {
        return region2;
    }

    /**
     * @return value dice number to get to pass through the cell
     */
    public int getValue() {
        return value;
    }

    /**
     * @return state of the cell
     */
    public short getState() {
        return state;
    }

    /**
     * @param state
     *            state to set
     */
    public void setState(short state) {
        this.state = state;
    }

}
