/**
 * 
 */
package it.polimi.cg16.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Michele
 * 
 */
public class Region {

    private int id;
    private List<Sheep> sheeps;
    private Terrain terrain;

    public Region(int id) {
        this.id = id;
        sheeps = new ArrayList<Sheep>();
    }

    /**
     * Create a new region
     * 
     * @param terrain
     *            type of the new region
     */
    public Region(int id, Terrain terrain) {
        sheeps = new ArrayList<Sheep>();
        this.id = id;
        this.terrain = terrain;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the sheep
     */
    public List<Sheep> getSheep() {
        return sheeps;
    }

    /**
     * Set a list of sheep to this region
     * 
     * @param sheeps
     *            list to be set
     */
    public void setSheeps(List<Sheep> sheeps) {
        this.sheeps = sheeps;
    }

    /**
     * add one more sheep
     */
    public void addSheep(Sheep sheep) {
        this.sheeps.add(sheep);
    }

    /**
     * remove one sheep
     */
    public void removeSheep(Sheep sheep) {
        this.sheeps.remove(sheep);
    }

    /**
     * @return the terrain type
     */
    public Terrain getTerrain() {
        return terrain;
    }

}
