/**
 * 
 */
package it.polimi.cg16.model;

/**
 * @author Andrea
 * 
 */
public class Card {
    private Terrain terrain;
    private int cost;

    /**
     * Initialize Card with terrain type and cost
     * 
     * @param terrain
     * @param cost
     * */
    public Card(Terrain terrain, int cost) {
        this.terrain = terrain;
        this.cost = cost;
    }

    /**
     * @return terrain the terrain of the card
     */
    public Terrain getTerrain() {
        return terrain;
    }

    /**
     * * @return cost the cost of the card
     */
    public int getCost() {
        return cost;

    }

}
