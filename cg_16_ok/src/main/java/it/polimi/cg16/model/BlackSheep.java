/**
 * 
 */
package it.polimi.cg16.model;

/**
 * @author Michele
 * 
 */
public class BlackSheep extends Sheep {
    private boolean killed;

    public BlackSheep(int id, Region region) {
        super(id, region);
    }

    /**
     * Set the state of the blacksheep
     * 
     * @param killed
     */
    public void setKilled(boolean killed) {
        this.killed = killed;
    }

    /**
     * 
     * @return true if the blacksheep is killed, false otherwise
     */
    public boolean isKilled() {
        return killed;
    }

}
