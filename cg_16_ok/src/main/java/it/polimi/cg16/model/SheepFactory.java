/**
 * 
 */
package it.polimi.cg16.model;

/**
 * @author Michele
 * 
 */
public class SheepFactory {
    private static int id = 18;

    /**
     * 
     * @param region
     * @return a sheep with a static incremented id
     */
    public Sheep getSheep(Region region) {
        id++;
        return new Sheep(id, region);
    }
}
