/**
 * 
 */
package it.polimi.cg16.model;

/**
 * @author Andrea & Michele
 * 
 */
public class Shepherd {
    private Cell cell;
    private Color color;

    public Shepherd(Color color) {
        this.color = color;
    }

    /**
     * @return the cell of the shepherd
     */
    public Cell getCell() {
        return cell;
    }

    /**
     * @param cell
     *            the cell to set
     */
    public void setCell(Cell cell) {
        this.cell = cell;
    }

    /**
     * @return the color of the shepherd
     */
    public Color getColor() {
        return color;
    }

}
