/**
 * 
 */
package it.polimi.cg16.model;

import java.util.Random;

/**
 * @author Andrea & Michele
 * 
 */
public class Dice {

    /**
     * @return extractedNum the extracted casual number
     */
    public int roll() {
        Random randomNumber;
        randomNumber = new Random();
        return randomNumber.nextInt(5) + 1;
    }

}
