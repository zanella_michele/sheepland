/**
 * 
 */
package it.polimi.cg16.model;

/**
 * @author Michele
 * 
 */
public class Sheep {
    protected Region region;
    private int id;

    public Sheep(int id, Region region) {
        this.id = id;
        this.region = region;
    }

    /**
     * @return the region where is the black sheep
     */
    public Region getRegion() {
        return region;
    }

    /**
     * @param region
     *            the region to set
     */
    public void setRegion(Region region) {
        this.region = region;
    }

    public int getId() {
        return id;
    }
}
