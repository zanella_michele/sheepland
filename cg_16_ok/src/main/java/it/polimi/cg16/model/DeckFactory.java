/**
 * 
 */
package it.polimi.cg16.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Michele
 * 
 */
public class DeckFactory {

	Terrain terrain;

	/**
	 * Create a deck with all the cards and the specific cost
	 * 
	 * @param numberOfCardsPerTerrain
	 * @return deck
	 */
	public List<Card> deckCreator(int numberOfCardsPerTerrain) {
		List<Card> deck = new ArrayList<Card>();
		for (Terrain t : Terrain.values()) {
			for (int cost = 0; cost < numberOfCardsPerTerrain; cost++) {
				deck.add(new Card(t, cost));
			}
		}
		return deck;
	}

}
