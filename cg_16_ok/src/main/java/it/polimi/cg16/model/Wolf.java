/**
 * 
 */
package it.polimi.cg16.model;

/**
 * @author Michele
 * 
 */
public class Wolf {
    protected Region region;

    public Wolf(Region region) {
        this.region = region;
    }

    /**
     * @return the region where is the wolf
     */
    public Region getRegion() {
        return region;
    }

    /**
     * @param region
     *            the new region of the wolf
     */
    public void setRegion(Region region) {
        this.region = region;
    }
}
