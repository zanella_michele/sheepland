/**
 * This package contains the <strong>model</strong> part.
 * Contains all physical parts of the game such as cards, sheep and gameboard.
 */

package it.polimi.cg16.model;