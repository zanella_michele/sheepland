/**
 * 
 */
package it.polimi.cg16.model;

/**
 * @author Michele
 * 
 */
public enum Color {
    RED(0, "Red"), BLUE(1, "Blue"), GREEN(2, "Green"), YELLOW(3, "Yellow");

    private int colorId;
    private String colorName;

    private Color(int colorId, String colorName) {
        this.colorId = colorId;
        this.colorName = colorName;
    }

    /**
     * 
     * @return the int id of the color
     */
    public int getId() {
        return colorId;
    }

    /**
     * 
     * @return a string description of the color
     */
    public String getName() {
        return colorName;
    }
}
