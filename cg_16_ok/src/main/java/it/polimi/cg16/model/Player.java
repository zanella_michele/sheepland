/**
 * 
 */
package it.polimi.cg16.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Michele
 * 
 */
public class Player {
    private List<Card> terrainCards;
    private List<Shepherd> shepherd;
    private int money;
    private int points;

    public class NoMoneyException extends Exception {

        /**
         * 
         */
        private static final long serialVersionUID = -8877223085315173097L;
        private final Player player;

        public NoMoneyException(Player player) {
            this.player = player;
        }

        public Player getPlayer() {
            return player;
        }
    }

    public Player() {
        terrainCards = new ArrayList<Card>();
        shepherd = new ArrayList<Shepherd>();
        money = 0;
    }

    /**
     * 
     * @return terrainCards
     */
    public List<Card> getTerrainCards() {
        return terrainCards;
    }

    /**
     * Add a specific terrainCard
     * 
     * @param terrainType
     */
    public void addTerrainCard(Card terrainCard) {
        this.terrainCards.add(terrainCard);
    }

    /**
     * @return the shepherds list
     */
    public List<Shepherd> getShepherd() {
        return shepherd;
    }

    /**
     * @param shepherd
     *            the shepherd to add
     */
    public void addShepherd(Shepherd shepherd) {
        this.shepherd.add(shepherd);
    }

    /**
     * @return the money
     */
    public int getMoney() {
        return money;
    }

    /**
     * @param money
     *            the money to set
     */
    public void setMoney(int money) {
        this.money = money;
    }

    /**
     * Remove cost from money
     * 
     * @param cost
     * @throws NoMoneyException
     */
    public void removeMoney(int cost) throws NoMoneyException {
        // Check if shepherd has enough money to buy the card
        if (money - cost >= 0) {
            money = money - cost;
        } else {
            throw new NoMoneyException(this);
        }
    }

    /**
     * @return the points
     */
    public int getPoints() {
        return points;
    }

    /**
     * @param points
     *            the points to set
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * 
     * @param terrain
     * @return the number of cards of a specific terrain
     */
    public int getNumberOfCards(Terrain terrain) {
        int num = 0;
        for (Card c : terrainCards) {
            if (c.getTerrain() == terrain) {
                num++;
            }
        }
        return num;
    }
}
