/**
 * 
 */
package it.polimi.cg16.model;

/**
 * @author Michele
 * 
 */
public enum Terrain {
    PLAIN(0, "Plain"), WOOD(1, "Wood"), SWAMP(2, "Swamp"), COUNTRY(3, "Country"), MOUNTAIN(4, "Mountain"), DESERT(5, "Desert");

    private int type;
    private String typeName;

    /**
     * Creator of Terrain
     * 
     * @param type
     * @param typeName
     */
    private Terrain(int type, String typeName) {
        this.type = type;
        this.typeName = typeName;
    }

    /**
     * 
     * @return typeName string with the name of the type
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * 
     * @return type the constant of the type
     */
    public int getTypeConstant() {
        return type;
    }

}
