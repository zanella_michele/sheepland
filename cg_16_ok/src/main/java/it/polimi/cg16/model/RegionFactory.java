/**
 * 
 */
package it.polimi.cg16.model;

import static it.polimi.cg16.model.Terrain.COUNTRY;
import static it.polimi.cg16.model.Terrain.DESERT;
import static it.polimi.cg16.model.Terrain.MOUNTAIN;
import static it.polimi.cg16.model.Terrain.PLAIN;
import static it.polimi.cg16.model.Terrain.SWAMP;
import static it.polimi.cg16.model.Terrain.WOOD;

/**
 * @author Andrea
 * 
 */
public class RegionFactory {

    /**
     * Create a specific set of region
     * 
     * @param numberOfRegion
     * @return
     */
    public Region[] regionCreator() {
        Region[] region;

        region = new Region[18 + 1];
        // Initialization of Sheepsburg
        region[0] = new Region(0);
        // Initialization of regions
        for (int i = 1; i <= 3; i++) {
            region[i] = new Region(i, PLAIN);
            region[i].addSheep(new Sheep(i, region[i]));
            region[i + 3] = new Region(i + 3, WOOD);
            region[i + 3].addSheep(new Sheep(i + 3, region[i + 3]));
            region[i + 6] = new Region(i + 6, SWAMP);
            region[i + 6].addSheep(new Sheep(i + 6, region[i + 6]));
            region[i + 9] = new Region(i + 9, COUNTRY);
            region[i + 9].addSheep(new Sheep(i + 9, region[i + 9]));
            region[i + 12] = new Region(i + 12, MOUNTAIN);
            region[i + 12].addSheep(new Sheep(i + 12, region[i + 12]));
            region[i + 15] = new Region(i + 15, DESERT);
            region[i + 15].addSheep(new Sheep(i + 15, region[i + 15]));
        }
        return region;

    }

}
