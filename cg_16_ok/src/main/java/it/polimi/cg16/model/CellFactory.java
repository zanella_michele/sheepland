/**
 * 
 */
package it.polimi.cg16.model;

/**
 * @author Andrea
 * 
 */
public class CellFactory {

    /**
     * Create a specific set of cells
     * 
     * @param region
     * @param numberOfCell
     * @return
     */
    public Cell[] cellCreator(Region[] region) {
        Cell[] cell;

        // Initialization of cells
        cell = new Cell[42];
        cell[0] = new Cell(0, region[1], region[11], 2);
        cell[1] = new Cell(1, region[11], region[12], 3);
        cell[2] = new Cell(2, region[12], region[14], 1);
        cell[3] = new Cell(3, region[12], region[13], 2);
        cell[4] = new Cell(4, region[11], region[13], 6);
        cell[5] = new Cell(5, region[11], region[10], 4);
        cell[6] = new Cell(6, region[1], region[10], 3);
        cell[7] = new Cell(7, region[1], region[2], 1);
        cell[8] = new Cell(8, region[2], region[10], 2);
        cell[9] = new Cell(9, region[10], region[13], 5);
        cell[10] = new Cell(10, region[13], region[14], 3);
        cell[11] = new Cell(11, region[14], region[15], 2);
        cell[12] = new Cell(12, region[14], region[16], 5);
        cell[13] = new Cell(13, region[13], region[16], 4);
        cell[14] = new Cell(14, region[13], region[0], 1);
        cell[15] = new Cell(15, region[10], region[0], 6);
        cell[16] = new Cell(16, region[10], region[3], 1);
        cell[17] = new Cell(17, region[2], region[3], 3);
        cell[18] = new Cell(18, region[2], region[4], 4);
        cell[19] = new Cell(19, region[4], region[3], 2);
        cell[20] = new Cell(20, region[3], region[0], 5);
        cell[21] = new Cell(21, region[0], region[16], 2);
        cell[22] = new Cell(22, region[16], region[15], 3);
        cell[23] = new Cell(23, region[15], region[17], 1);
        cell[24] = new Cell(24, region[17], region[16], 6);
        cell[25] = new Cell(25, region[16], region[9], 1);
        cell[26] = new Cell(26, region[0], region[9], 3);
        cell[27] = new Cell(27, region[0], region[6], 4);
        cell[28] = new Cell(28, region[3], region[6], 6);
        cell[29] = new Cell(29, region[3], region[5], 4);
        cell[30] = new Cell(30, region[4], region[5], 1);
        cell[31] = new Cell(31, region[5], region[6], 3);
        cell[32] = new Cell(32, region[6], region[9], 5);
        cell[33] = new Cell(33, region[9], region[17], 2);
        cell[34] = new Cell(34, region[17], region[18], 5);
        cell[35] = new Cell(35, region[18], region[9], 4);
        cell[36] = new Cell(36, region[9], region[8], 6);
        cell[37] = new Cell(37, region[6], region[8], 2);
        cell[38] = new Cell(38, region[6], region[7], 1);
        cell[39] = new Cell(39, region[5], region[7], 2);
        cell[40] = new Cell(40, region[7], region[8], 5);
        cell[41] = new Cell(41, region[8], region[18], 1);

        return cell;

    }

}
